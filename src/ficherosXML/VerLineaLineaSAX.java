package ficherosXML;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

public class VerLineaLineaSAX {

	public static void main(String[] args) {
		String ruta = "/home/alumno/biblioteca/";

		System.out.println("=============== SAX ===============");

		String tituloLibro = Entrada.cadenaVal("Introduzca título: ", "Introduzca un titulo valido", ".+", false);

		XMLReader reader = null;
		try {
			// Creamos la factoria analizadores por defecto
			reader = XMLReaderFactory.createXMLReader();
			// Añadimos nuestro manejador al reader
			reader.setContentHandler(new ManejadorEjemplo(tituloLibro, -1));
			// Procesamos el xml de ejemplo
			reader.parse(new InputSource(ruta + "IndexaTextos"));

			if (!((ManejadorEjemplo) reader.getContentHandler()).isEsTitulo()) {
				System.out.println("El libro " + tituloLibro + " no existe");
				return;
			}

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int lineaLibro = Entrada.enteroVal("Introduza linea: ", "Introduzca un numero valido, y mayor de 0", 0,
				Integer.MAX_VALUE, false);

		try {
			// Añadimos nuestro manejador al reader
			reader.setContentHandler(new ManejadorEjemplo(tituloLibro, lineaLibro));
			// Procesamos el xml de ejemplo
			reader.parse(new InputSource(ruta + "IndexaTextos"));

			if (!((ManejadorEjemplo) reader.getContentHandler()).esCorrecto) {
				System.out.println("La pagina " + lineaLibro + " del libro " + tituloLibro + " no existe");
				return;
			}

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		RandomAccessFile lecturaFichero = null;
		try {
			lecturaFichero = new RandomAccessFile(ruta + tituloLibro, "rw");
			lecturaFichero.seek(Integer.parseInt(((ManejadorEjemplo) reader.getContentHandler()).getResultado()));
			System.out.println(lecturaFichero.readLine());
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el fichero");
			return;

		} catch (IOException e1) {
			e1.printStackTrace();
		}finally {
			try {
				lecturaFichero.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}

class ManejadorEjemplo extends DefaultHandler {

	String titulo;
	int linea;// -1 si no queremos buscar linea
	boolean tituloExiste;
	boolean lineaEncontrada;
	boolean esCorrecto;
	String resultado;

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public boolean isEsTitulo() {
		return tituloExiste;
	}

	public void setEsTitulo(boolean esTitulo) {
		this.tituloExiste = esTitulo;
	}

	public boolean isEsLinea() {
		return lineaEncontrada;
	}

	public void setEsLinea(boolean esLinea) {
		this.lineaEncontrada = esLinea;
	}

	public ManejadorEjemplo(String titulo, int linea) {
		super();
		this.titulo = titulo;
		this.linea = linea;
		tituloExiste = false;
		lineaEncontrada = false;
		esCorrecto = false;
	}

	@Override
	public void startDocument() throws SAXException {

	}

	public void endDocument() throws SAXException {

	}

	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
		if (attributes.getLength() != 0 && attributes.getValue(0).equals(titulo)) {
			tituloExiste = true;
		}

		if (tituloExiste && attributes.getValue(0).equals(String.valueOf(linea))) {
			lineaEncontrada = true;
		}
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		if (lineaEncontrada && tituloExiste) {

			resultado = String.valueOf(ch, start, length);

			lineaEncontrada = false;
			tituloExiste = false;
			esCorrecto = true;
		}
	}

	public void endElement(String uri, String localName, String name) throws SAXException {
		if (localName.equals(titulo)) {
			tituloExiste = false;
		}
	}
}
