package ficherosXML;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class EjercicioXStreamGuardar {

	public static void main(String[] args) {
		// Creacion de objetos persona
		Persona p1 = new Persona(20, "Julio Perez", 'M');
		p1.añadirTrabajo(new Trabajo(1, "Albanil"));
		p1.añadirTrabajo(new Trabajo(1, "Encofrador"));
		Persona p2 = new Persona(21, "Ana Gutierrez", 'F');
		p2.añadirTrabajo(new Trabajo(2, "Ingeniero"));
		Persona p3 = new Persona(84, "Emilia Rivilla", 'F');
		p3.añadirTrabajo(new Trabajo(2, "Profesora"));
		p3.añadirTrabajo(new Trabajo(3, "Locutora de radio"));
		// Fin de creacion de objetos persona

		ArrayList<Persona> personas = new ArrayList<>();

		PersonasArray personasArray = new PersonasArray(personas);
		
		personas.add(p1);
		personas.add(p2);
		personas.add(p3);

		// Se crea el objeto XStream
		// XStream xstream = new XStream();

		XStream xstream = new XStream(new DomDriver());

		// Se establecen los alias
		xstream.alias("Persona", Persona.class);
		xstream.alias("Trabajo", Trabajo.class);
		xstream.alias("Personas", PersonasArray.class);

		// Se establecen las colecciones
		xstream.addImplicitCollection(Persona.class, "trabs");
		xstream.addImplicitCollection(personasArray.getClass(), "personas");

		String xml ="<?xml version =\"1.0\"?>\n";
		
		// Se pasa a String
		xml += xstream.toXML(personasArray);

		// Se muestra el String
		System.out.println(xml);
		
		// Se guarda en un fichero
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(new File("xmlXStream.xml")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pw.print(xml);
		
		pw.close();
	}

}
