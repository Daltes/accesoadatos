package ficherosXML;

//Muestra el contenido de un fichero XML utilizando el parser SAX

import java.io.*;
import java.util.Scanner;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
* Manejador de eventos SAX de ejemplo.
* 
* 
*/
class ManejadorEjemploa extends DefaultHandler {

	@Override
	public void startDocument() throws SAXException {
		System.out.println("\nPrincipio del documento...");
	}

	public void endDocument() throws SAXException {
		System.out.println("\nFin del documento...");
	}

	public void startElement(String uri, String localName, String name,
			Attributes attributes) throws SAXException {
		System.out.println("\nComienzo etiqueta:" + localName);

		// Recorremos los atributos
		System.out.println("\t" + attributes.getLength()
				+ " atributos...");
		for (int i = 0; i < attributes.getLength(); i++) {
			System.out.println("\t\tNombre: " + attributes.getLocalName(i));
			System.out.println("\t\tValor: " + attributes.getValue(i));
		}

		// También podemos obtener los atributos por nombre
		String valorId = attributes.getValue("id");
		if (valorId != null) {
			System.out.println("\tId: " + valorId);
		}

	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {

		System.out.println("\tTexto: " + String.valueOf(ch, start, length));
	}

	public void endElement(String uri, String localName, String name)
			throws SAXException {
		System.out.println("\nFin de etiqueta:" + localName);
	}
}

/**
* Clase que procesa un XML de ejemplo mediante el handler SAX ManejadorEjemplo
* 
* 
*/
public class EjemploSAX {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.print("Introduzca path fichero XML:");
		String path_xml = s.next();
		try {
			// Creamos la factoria analizadores por defecto
			XMLReader reader = XMLReaderFactory.createXMLReader();
			// Añadimos nuestro manejador al reader
			reader.setContentHandler(new ManejadorEjemploa());
			// Procesamos el xml de ejemplo
			reader.parse(new InputSource(path_xml));
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		s.close();
	}

}
