package ficherosXML;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class EjercicioXStreamLeer {

	public static void main(String[] args) {

		// Se establece el canal de lectura
		BufferedReader bfr = null;
		try {
			bfr = new BufferedReader(new FileReader(new File("xmlXStream.xml")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Se lee el fichero
		String xml = "";
		String linea;
		try {
			while ((linea = bfr.readLine()) != null) {
				xml += linea;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				bfr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// Se muestra el String leido del fichero
		System.out.println(xml);

		// Se crea el XStream
		XStream xstream = new XStream(new DomDriver());

		// Se establecen los alias
		xstream.alias("Persona", Persona.class);
		xstream.alias("Trabajo", Trabajo.class);
		xstream.alias("Personas", PersonasArray.class);

		// Se establecen las colecciones
		xstream.addImplicitCollection(Persona.class, "trabs");
		xstream.addImplicitCollection(PersonasArray.class, "personas");

		// Se transforma el xml en un objeto PersonasArray
		PersonasArray personas = (PersonasArray) xstream.fromXML(xml);

		// Se crean los objetos para comparar
		Persona persona1Original = new Persona(20, "Julio Perez", 'M');
		persona1Original.añadirTrabajo(new Trabajo(1, "Albanil"));
		persona1Original.añadirTrabajo(new Trabajo(1, "Encofrador"));
		Persona persona2Original = new Persona(21, "Ana Gutierrez", 'F');
		persona2Original.añadirTrabajo(new Trabajo(2, "Ingeniero"));
		Persona persona3Original = new Persona(84, "Emilia Rivilla", 'F');
		persona3Original.añadirTrabajo(new Trabajo(2, "Profesora"));
		persona3Original.añadirTrabajo(new Trabajo(3, "Locutora de radio"));

		// Se recogen las personas del objeto PersonasArray
		Persona persona1CopiaFichero = personas.getPersonas().get(0);
		Persona persona2CopiaFichero = personas.getPersonas().get(1);
		Persona persona3CopiaFichero = personas.getPersonas().get(2);

		
		// Se comparan
		System.out.println(persona1Original);
		System.out.println(persona1CopiaFichero);

		System.out.println("=========================");

		System.out.println(persona2Original);
		System.out.println(persona2CopiaFichero);

		System.out.println("=========================");

		System.out.println(persona3Original);
		System.out.println(persona3CopiaFichero);

		System.out.println("=========================");
	}

}
