package ficherosXML;

import java.util.ArrayList;

public class PersonasArray {
	ArrayList<Persona> personas;

	public ArrayList<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(ArrayList<Persona> personas) {
		this.personas = personas;
	}

	public PersonasArray(ArrayList<Persona> personas) {
		super();
		this.personas = personas;
	}
	
}
