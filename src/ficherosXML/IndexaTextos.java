package ficherosXML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class IndexaTextos {

	public static void main(String[] args) {

		// Creamos el arbol DOM de XML
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		Document doc = docBuilder.newDocument();
		// Fin de creacion de arbol DOM

		// Creacion del elemento RAIZ
		Element raiz = doc.createElement("biblioteca");
		doc.appendChild(raiz);
		// Fin de creacion de RAIZ

		// Creamos el acceso al fichero para leerlo

		File carpeta = new File("/home/alumno/biblioteca");

		File[] archivos = carpeta.listFiles();

		RandomAccessFile lecturaFichero = null;
		
		ArrayList<Element> libros = new ArrayList<>();
		
		for (int i = 0; i < archivos.length; i++) {

			try {
				lecturaFichero = new RandomAccessFile(archivos[i].getAbsolutePath(), "rw");
			} catch (FileNotFoundException e) {
				System.out.println("No se ha encontrado el fichero");
			}

			// Creacion del elemento libro
			libros.add(doc.createElement("libro"));
			libros.get(i).setAttribute("titulo", archivos[i].getName());
			raiz.appendChild(libros.get(i)); // Se vincula a raiz
			// Fin de creacion de elemento hijo

			int cntLinea = 0;

			// Creacion del primer nodo hijo de Libro
			ArrayList<Element> lineas = new ArrayList<>();
			

			@SuppressWarnings("unused")
			String linealeida;
			boolean esPrimeraVez = true;
			try {
				while ((linealeida = lecturaFichero.readLine()) != null) {

					if (esPrimeraVez) {
						lecturaFichero.seek(0);
						esPrimeraVez = false;
					}

					cntLinea++;
					lineas.add(doc.createElement("linea"));
					lineas.get(cntLinea - 1).setAttribute("num", String.valueOf(cntLinea));
					libros.get(i).appendChild(lineas.get(cntLinea - 1));
					lineas.get(cntLinea - 1).setTextContent(String.valueOf(lecturaFichero.getFilePointer()));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		// Fin de creacion de nodo de Libro

		
		// Creacion de fichero XML
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e1) {
			e1.printStackTrace();
		}
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("/home/alumno/biblioteca/IndexaTextos"));
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		// Fin de creacion XML
		
		System.out.println("Se ha creado sastisfactoriamente");
	}

}
