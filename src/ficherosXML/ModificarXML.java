package ficherosXML;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ModificarXML {

	public static void main(String[] args) {

		String ruta = "./";
		String moneda = "euro";

		// Se abre el documento
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document doc = null;

		try {
			doc = docBuilder.parse(ruta + "desayunos.xml");
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		// Fin de abertura de documento

		// Borrar descripcion
		NodeList nodosDescripcion = doc.getElementsByTagName("descripcion");
		for (int i = 0; i < nodosDescripcion.getLength(); i++) {
			nodosDescripcion.item(i).getParentNode().removeChild(nodosDescripcion.item(i));
		}
		// Fin de borrar descripion
		
		// Sumar calorias
		NodeList nodosCalorias = doc.getElementsByTagName("calorias");
		int caloriasTotal = 0;
		for (int i = 0; i < nodosCalorias.getLength(); i++) {
			caloriasTotal += Integer.parseInt(nodosCalorias.item(i).getTextContent());
		}
		// Fin sumar calorias
		
		// Añadir elemento total_calorias
		Element raiz = null;
		raiz = (Element) doc.getFirstChild();
		Element nodoCaloriasTotal = doc.createElement("total_calorias");
		nodoCaloriasTotal.setTextContent(String.valueOf(caloriasTotal));
		raiz.appendChild(nodoCaloriasTotal);
		
		// Leer divisas.csv
		double valorDivisa = buscarValorDivisa(moneda);
		// Fin Leer Divisas.csv
		
		// Cambiar divisas y precio en nodos
		NodeList nodosPrecios = doc.getElementsByTagName("precio");
		
		for (int i = 0; i < nodosPrecios.getLength(); i++) {
			
			if(valorDivisa != -1) {
				double nuevoPrecio = valorDivisa * 
						(Double.parseDouble(nodosPrecios.item(i).getTextContent()) / buscarValorDivisa(nodosPrecios.item(i).getAttributes().getNamedItem("moneda").getTextContent()));
				nodosPrecios.item(i).setTextContent(new DecimalFormat("0.00").format(nuevoPrecio));
			}
			nodosPrecios.item(i).getAttributes().getNamedItem("moneda").setNodeValue(moneda);
		}
		// Fin cambiar divisas y precio en nodos
		
		// Guardar fichero
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e1) {
			e1.printStackTrace();
		}
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(ruta + "desayunosModificado.xml"));
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		// Fin guardar fichero
		
		System.out.println("Se ha creado el fichero desayudoModificado.xml");
	}

	private static double buscarValorDivisa(String nombre) {
		BufferedReader leerFichero = null;
		String rutaDivisas = "divisas.csv";
		double valorDivisa = -1;

		try {
			leerFichero = new BufferedReader(new FileReader(rutaDivisas));
		} catch (FileNotFoundException e) {
			System.out.println("Error al leer fichero");
		}

		String linea = "";
		try {
			while ((linea = leerFichero.readLine()) != null) {
				String[] lineaDelFichero = linea.split(";");
				if(lineaDelFichero[0].equals(nombre)) {
					valorDivisa = Double.parseDouble(lineaDelFichero[1]);
					return valorDivisa;
				}
			}
		} catch (IOException e) {
			System.out.println("Error al leer el fichero");
		}
		return -1;
	}
	
	public static void mostrarXML(Document doc) 
			  throws TransformerException {
			    Transformer transformer = TransformerFactory.newInstance().newTransformer();
			    transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			    StreamResult result = new StreamResult(new StringWriter());
			    DOMSource source = new DOMSource(doc);
			    transformer.transform(source, result);

			    String xmlString = result.getWriter().toString();
			    System.out.println(xmlString);
			  }

}
