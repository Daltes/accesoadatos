package ficherosXML;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class VerLineaLibro {

	public static void main(String[] args) {

		String ruta = "/home/alumno/biblioteca/";

		// Se abre el documento
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document doc = null;

		try {
			doc = docBuilder.parse(ruta + "IndexaTextos");
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		// Fin de abertura de documento

		// Se crea xpath

		String tituloLibro = Entrada.cadenaVal("Introduzca título: ", "Introduzca un titulo valido", ".+", false);
		Element libro = null;
		try {
			libro = (Element) (XPathFactory.newInstance().newXPath()
					.evaluate("/biblioteca/libro[@titulo='" + tituloLibro + "']", doc, XPathConstants.NODE));

			if (libro == null) {
				System.out.println("El libro no existe");
				System.exit(0);
			}

		} catch (XPathExpressionException e1) {
			e1.printStackTrace();
		}

		int lineaLibro = Entrada.enteroVal("Introduza linea: ", "Introduzca un numero valido, y mayor de 0", 0,
				Integer.MAX_VALUE, false);

		int numero = 0;
		try {
			Element linea = (Element) (XPathFactory.newInstance().newXPath()
					.evaluate("linea[@num='" + String.valueOf(lineaLibro) + "']", libro, XPathConstants.NODE));

			if (linea == null) {
				System.out.println("No existe esa linea");
				System.exit(0);
			}
			numero = Integer.parseInt(linea.getTextContent());
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		RandomAccessFile lecturaFichero = null;
		try {
			lecturaFichero = new RandomAccessFile(ruta + tituloLibro, "rw");
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el fichero");
		}

		try {
			lecturaFichero.seek(numero);
			System.out.println("=============== DOM ===============");
			System.out.println(lecturaFichero.readLine());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}

