package sql2;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sun.org.apache.bcel.internal.generic.CPInstruction;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;

public class InterfazUsuarioGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	GestorBBDD bbdd;
	private String ipServidor;
	private String usuario;
	private String contrasena;
	private JButton btnDesconectar;
	private JComboBox<String> comboBox_BaseDatos;
	private JList list_Columnas;
	private JComboBox<String> comboBox_Tablas;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnConectar = new JButton("Conectar");
		btnConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				conectarBBDD();
				comboBox_BaseDatos.removeAllItems();
				comboBox_Tablas.removeAllItems();
				list_Columnas.removeAll();
				btnConectar.setEnabled(false);
				btnDesconectar.setEnabled(true);
				comboBox_BaseDatos.setEnabled(true);
				rellenarComboBox();
			}
		});
		btnConectar.setBounds(12, 12, 117, 25);
		contentPane.add(btnConectar);

		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					bbdd.deconectarDB();
				} catch (Exception e1) {
					notificaError(null, "Error al desconectar", e1, "Se ha producido un error al desconectar la bbdd");
				}

				btnConectar.setEnabled(true);
				btnDesconectar.setEnabled(false);
				comboBox_BaseDatos.setEnabled(false);
				comboBox_Tablas.setEnabled(false);

			}
		});
		btnDesconectar.setEnabled(false);
		btnDesconectar.setBounds(141, 12, 148, 25);
		contentPane.add(btnDesconectar);

		comboBox_BaseDatos = new JComboBox();
		comboBox_BaseDatos.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				rellenarTablas();
			}
		});
		comboBox_BaseDatos.setEnabled(false);
		comboBox_BaseDatos.setBounds(12, 50, 148, 24);
		contentPane.add(comboBox_BaseDatos);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 90, 332, 133);
		contentPane.add(scrollPane);

		list_Columnas = new JList();
		scrollPane.setViewportView(list_Columnas);

		comboBox_Tablas = new JComboBox();
		comboBox_Tablas.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				rellenarColumnas();
			}
		});
		comboBox_Tablas.setEnabled(false);
		comboBox_Tablas.setBounds(196, 49, 148, 25);
		contentPane.add(comboBox_Tablas);
	}

	@SuppressWarnings("unchecked")
	protected void rellenarColumnas() {

		ArrayList<Columna> columnas;

		list_Columnas.removeAll();

		try {
			columnas = bbdd.listarColumnas((String) comboBox_BaseDatos.getSelectedItem(),
					(String) comboBox_Tablas.getSelectedItem());
		} catch (SQLException e) {
			notificaError(null, "Error al listar", e,
					"Se ha producido un error al realizar una busqueda de las Bases de Datos");
			return;
		}

		Vector<Object> resultadoNombre = new Vector<>();

		for (int i = 0; i < columnas.size(); i++) {
			resultadoNombre.add(columnas.get(i).getNombre() + " - " + columnas.get(i).getTipo());
		}

		list_Columnas.setListData(resultadoNombre);

	}

	protected void rellenarTablas() {

		ArrayList<String> tablas;

		comboBox_Tablas.removeAllItems();

		try {
			tablas = bbdd.listarTablas((String) comboBox_BaseDatos.getSelectedItem());
		} catch (SQLException e) {
			notificaError(null, "Error al listar", e,
					"Se ha producido un error al realizar una busqueda de las Bases de Datos");
			return;
		}

		for (int i = 0; i < tablas.size(); i++) {
			comboBox_Tablas.addItem(tablas.get(i));
		}

		comboBox_Tablas.setEnabled(true);

	}

	protected void rellenarComboBox() {
		ArrayList<String> basesDatos;
		try {
			basesDatos = bbdd.listarBases();
		} catch (SQLException e) {
			notificaError(null, "Error al listar", e,
					"Se ha producido un error al realizar una busqueda de las Bases de Datos");
			return;
		}

		for (int i = 0; i < basesDatos.size(); i++) {
			comboBox_BaseDatos.addItem(basesDatos.get(i));
		}

	}

	private void conectarBBDD() {
		// Se conecta con la base de datos
		bbdd = new GestorBBDD();
		// Se lee el fichero CFG.INI para conectarse a la bbdd
		BufferedReader bf = null;
		try {
			bf = new BufferedReader(new FileReader(new File("CFG2.INI")));
			ipServidor = bf.readLine();
			usuario = bf.readLine();
			contrasena = bf.readLine();

		} catch (IOException e) {
			notificaError(this, "Error con fichero CFG.INI", e, "No se ha podido leer el fichero CFG.INI");
		} finally {
			if (bf != null)
				try {
					bf.close();
				} catch (IOException e) {
					notificaError(this, "Error al cerrar el fichero", e, "No se ha podido cerrar el fichero CFG.INI");
				}
		}

		// Se establece la conexion
		try {
			bbdd.conectarDB(ipServidor, usuario, contrasena);
		} catch (Exception e) {
			notificaError(this, "Error a conectar", e, "No se ha podido conectar con la base de datos");
		}
	}

	public static void notificaError(JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido = "";
		if (e != null)
			contenido += e.getClass().getName() + "\n" + e.getMessage(); // Nombre de la excepcion y mensaje de la
																			// excep.
		if (mensaje != null)
			contenido += mensaje;
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
	}
}
