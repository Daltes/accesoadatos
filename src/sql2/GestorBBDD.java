package sql2;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestorBBDD {

	private Connection con;
	private DatabaseMetaData metaDatos;

	public void conectarDB(String IP, String usu, String pass) throws Exception {

		Class.forName("com.mysql.jdbc.Driver");

		con = DriverManager.getConnection("jdbc:mysql://" + IP, usu, pass);

		metaDatos = con.getMetaData();
	}

	public void deconectarDB() throws Exception {
		con.close();
	}

	public ArrayList<String> listarBases() throws SQLException {

		ArrayList<String> resultado = new ArrayList<>();

		ResultSet bbdd = metaDatos.getCatalogs();

		while (bbdd.next()) {
			resultado.add(bbdd.getString("TABLE_CAT"));
		}

		return resultado;
	}

	public ArrayList<String> listarTablas(String base) throws SQLException {

		ArrayList<String> resultado = new ArrayList<>();

		ResultSet bbdd = metaDatos.getTables(base, null, "%", null);

		while (bbdd.next()) {
			resultado.add(bbdd.getString("TABLE_NAME"));
		}

		return resultado;
	}

	public ArrayList<Columna> listarColumnas(String base, String tabla) throws SQLException {

		ArrayList<Columna> resultado = new ArrayList<>();

		ResultSet bbdd = metaDatos.getColumns(base, null, tabla, "%");

		while (bbdd.next()) {
			resultado.add(new Columna(bbdd.getString("COLUMN_NAME"),
					bbdd.getString("TYPE_NAME") + " (" + bbdd.getString("COLUMN_SIZE") + ")"));
		}

		return resultado;
	}

	boolean existeTabla(String base, String tabla) throws SQLException {

		ResultSet bbdd = metaDatos.getTables(base, null, tabla, null);

		if (bbdd.next()) {
			return true;
		}

		return false;
	}

	boolean existeBD(String base) throws SQLException {

		ResultSet bbdd = metaDatos.getTables(base, null, "%", null);

		if (bbdd.next()) {
			return true;
		}

		return false;
	}

}
