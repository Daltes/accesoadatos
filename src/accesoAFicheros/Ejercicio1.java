package accesoAFicheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ejercicio1.soluc_SistemaDeFicheros.Utilidades;

public class Ejercicio1 {
	public static void main(String[] args) {
		BufferedReader leerTeclado = new BufferedReader(new InputStreamReader(System.in));
		File ruta = null;
		boolean bien = false;

		do {
			bien = false;
			System.out.println("Ruta del fichero?");
			try {
				ruta = new File(leerTeclado.readLine().trim());
			} catch (IOException e) {
				Utilidades.notificaError(null, "Error al leer del teclado", e, null);
			}

			if (!ruta.exists()) {
				bien = true;
				System.out.println("Ruta incorrecta");
			}

		} while (bien);

		BufferedReader leerFichero = null;

		try {
			leerFichero = new BufferedReader(new FileReader(ruta));
		} catch (FileNotFoundException e) {
			System.out.println("Error al leer fichero");
		}

		String linea = "";
		try {
			while ((linea = leerFichero.readLine()) != null) {
				String[] lineaDelFichero = linea.split(";");
				for (int i = 0; i < lineaDelFichero.length; i++) {
					System.out.format("%-15s", lineaDelFichero[i]);
				}
				System.out.println();
			}
		} catch (IOException e) {
			System.out.println("Error al leer el fichero");
		}

	}
}
