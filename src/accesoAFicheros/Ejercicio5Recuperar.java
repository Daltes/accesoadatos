package accesoAFicheros;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class Ejercicio5Recuperar {

	public static void main(String[] args) {
		ArrayList<Ejercicio5Persona> personas = new ArrayList<Ejercicio5Persona>();

		System.out.println("Se procedera a recuperar los datos\nPulse intro para continuar...");
		Entrada.cadena();

		// Lectura BufferedReader
		BufferedReader bfr = null;
		try {
			bfr = new BufferedReader(new FileReader(new File("persona.txt")));

			String objeto = "";

			try {
				while ((objeto = bfr.readLine()) != null) {
					personas.add(new Ejercicio5Persona(objeto));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			try {
				if (bfr != null)
					bfr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Lectura DataInputStream
		DataInputStream dis = null;
		try {
			dis = new DataInputStream(new FileInputStream(new File("persona.bin")));
			for (;;) {
				personas.add(new Ejercicio5Persona(dis.readInt(), dis.readUTF(), dis.readChar()));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e) {

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (dis != null)
					dis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Lectura con ObjectInputStream
		ObjectInputStream ois = null;

		try {
			ois = new ObjectInputStream(new FileInputStream(new File("persona.obj")));

			for (;;) {

				// Lee todos los objetos
				try {
					personas.add((Ejercicio5Persona) ois.readObject());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

			}

		} catch (EOFException e) {

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ois != null)
					ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println("Se han leido " + personas.size() + "personas");

		for (int i = 0; i < personas.size(); i++) {
			System.out.println(personas.get(i));
		}
	}

}
