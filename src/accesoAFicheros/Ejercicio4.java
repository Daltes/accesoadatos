package accesoAFicheros;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio4 {

	public static void main(String[] args) {
		// Se comprueba numero de argumentos
		if (args.length < 2) {
			System.out.println("Faltan argumentos \n" + "'comando archivoOrigen archivoDestino'");
			return;
		}

		if (args.length > 2) {
			System.out.println("Has introducido demasiados argumentos \n" + "'comando archivoOrigen archivoDestino'");
			return;
		}
		// Fin de comprobacion

		// Se crean los Files con las rutas dadas en lso argumentos
		File archivoOrigen = new File(args[0]);
		File archivoDestino = new File(args[1]);
		// Fin de creacion de Files

		// Se comprueba que el archivo de origen existe
		if (!archivoOrigen.exists()) {
			System.out.println("El archivo que se pretende copiar no existe");
			return;
		}

		// Se comprueba que el fichero de origen no sea un directorio
		if (archivoOrigen.isDirectory()) {
			System.out.println("El archivo que se pretende copiar es un directorio");
			return;
		}

		// Se comprueba que el fichero de destino no sea un directorio
		if (archivoDestino.isDirectory()) {
			System.out.println("El archivo de detino es un directorio");
		}

		// Se comprueba que el archivo de destino existe, si existe se pide confirmacion
		// de sobreescritura
		if (archivoDestino.exists()) {
			System.out.println("El fichero de destino ya existe \n ¿Desea sobreescribir el archivo?(si/no)");
			BufferedReader leerTeclado = new BufferedReader(new InputStreamReader(System.in));
			String respuesta = "";
			boolean bien = false;

			do {
				bien = false;
				try {
					respuesta = leerTeclado.readLine().trim().toLowerCase();
				} catch (IOException e) {
					System.out.println("Error al leer desde el teclado");
				}

				if (!(respuesta.equals("si") || respuesta.equals("no"))) {
					bien = true;
					System.out.println("Escriba 'si' o 'no'");
				}

			} while (bien);

			if (respuesta.equals("no")) {
				System.out.println("No se ha efectuado la copia");
				return;
			}
		}
		// Se realiza la copia
		realizarCopia(archivoOrigen, archivoDestino);
	}

	private static void realizarCopia(File archivoOrigen, File archivoDestino) {
		// Indicador de cuando se ha llegado al final del fichero
		boolean fin = true;
		// int donde se guardará el byte leido
		int byteLeido = 0;
		// Lectura del archivo
		DataInputStream lecturaOrigen = null;
		// Escritura de la copia
		DataOutputStream lecturaDestino = null;
		try {
			// Se inicicalizan los archivos
			lecturaOrigen = new DataInputStream(new FileInputStream(archivoOrigen));
			lecturaDestino = new DataOutputStream(new FileOutputStream(archivoDestino));
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el fichero");
		}
		// Lectura y escritura del archivo byte a byte

		try {
			do {
				// Se lee el byte
				byteLeido = lecturaOrigen.read();
				// Se comprueba si es fin de fichero
				if (byteLeido == -1) {
					// Se sale si es fin de fichero
					fin = false;
				} else {
					// Si no es fin de fichero se escribe el byte en el archivo donde se copiara
					lecturaDestino.write(byteLeido);
				}
			} while (fin);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {

			try {
				// Se cierran los ficheros
				lecturaOrigen.close();
				lecturaDestino.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}

	}

}
