package accesoAFicheros;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

public class Ejercicio5Grabar {

	public static void main(String[] args) {
		Ejercicio5Persona p1 = new Ejercicio5Persona(20, "Julio Perez", 'M');
		Ejercicio5Persona p2 = new Ejercicio5Persona(21, "Ana Gutierrez", 'F');
		Ejercicio5Persona p3 = new Ejercicio5Persona(84, "Emilia Rivilla", 'F');

		String ruta = "persona";

		System.out.println("Se procedera a guardar los datos\nPulse intro para continuar...");
		Entrada.cadena();

		PrintWriter pw = null;
		DataOutputStream dos = null;
		ObjectOutputStream oos = null;

		try {
			// Escritura con PrintWriter
			pw = new PrintWriter(new FileWriter(new File(ruta + ".txt")));
			pw.println(p1.formatearParaGuardar());

			// Escritura con DataOutputStream
			dos = new DataOutputStream(new FileOutputStream(new File(ruta + ".bin")));
			dos.writeInt(p2.getEdad());
			dos.writeUTF(p2.getNombre());
			dos.writeChar((char) p2.getSexo());

			// Escritura con ObjectOutputStream
			oos = new ObjectOutputStream(new FileOutputStream(new File(ruta + ".obj")));
			oos.writeObject(p3);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null)
				pw.close();
			try {
				if (dos != null)
					dos.close();
				if (oos != null)
					oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println(".\n.\n");
		System.out.println("Se ha guardado correctamente los 3 objetos con el nombre: \n" + ruta + ".txt, " + ruta
				+ ".bin, " + ruta + ".obj");

	}

}
