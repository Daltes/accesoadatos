package accesoAFicheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import ejercicio1.soluc_SistemaDeFicheros.Utilidades;

public class Ejercicio2 {

	public static void main(String[] args) {
		BufferedReader leerTeclado = new BufferedReader(new InputStreamReader(System.in));
		File ruta = null;
		boolean bien = false;

		do {
			bien = false;
			System.out.println("Ruta del fichero donde se grabarán los registros:");
			try {
				ruta = new File(leerTeclado.readLine().trim());
			} catch (IOException e) {
				Utilidades.notificaError(null, "Error al leer del teclado", e, null);
			}

		} while (bien);

		String datos = "";
		String lectura = "";
		
		if(!ruta.exists()) {
			datos += "Matricula;Marca;precio\n";
		}
		
		do {
			bien = true;
			System.out.println("Matrícula (Intro para salir):");
			try {
				lectura = leerTeclado.readLine().trim();
			} catch (IOException e) {
				Utilidades.notificaError(null, "Error al leer del teclado", e, null);
			}
			if (lectura.length() != 0) {
				datos += lectura + ";";
				datos += validarLectura("Marca:") + ";";
				datos += validarLectura("Precio:") + "\n";
			} else {
				bien = false;
			}

		} while (bien);

		PrintWriter escribirEnFichero = null;
		try {
			escribirEnFichero = new PrintWriter(new FileWriter(ruta,true));
			escribirEnFichero.print(datos);
		} catch (IOException e) {
			Utilidades.notificaError(null, "Error al escribir en el teclado", e, null);
		} finally {
			escribirEnFichero.close();
		}

	}

	private static String validarLectura(String mensaje) {
		String resultado = "";
		BufferedReader leerTeclado = new BufferedReader(new InputStreamReader(System.in));
		boolean bien = false;

		do {
			bien = false;
			System.out.println(mensaje);
			try {
				resultado = leerTeclado.readLine().trim();
			} catch (IOException e) {
				Utilidades.notificaError(null, "Error al leer del teclado", e, null);
			}

			if (resultado.length() == 0) {
				bien = true;
			}
		} while (bien);
		return resultado;

	}

}
