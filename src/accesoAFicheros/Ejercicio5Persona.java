package accesoAFicheros;

import java.io.Serializable;

public class Ejercicio5Persona implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int edad; // >0
	private String nombre;
	private char sexo; // M-Masculino,F-Femenino

	// Constructor
	public Ejercicio5Persona(int edad, String nombre, char sexo) {
		this.edad = edad;
		this.nombre = nombre;
		this.sexo = sexo;
	}

	// Segundo Contructor
	public Ejercicio5Persona(String cadenaEntrada) {
		String[] cadenaSeparada = cadenaEntrada.split(";");
		if (!cadenaEntrada.matches("[0-9]+;[A-Za-znÑáéíóúÁÉÍÓÚ ]+;[MFmf]{1}")) {
			throw new IllegalArgumentException(" El formato de entrada no es correcto (edad;nombre;sexo)");
		}

		this.edad = Integer.parseInt(cadenaSeparada[0]);
		this.nombre = cadenaSeparada[1];
		this.sexo = cadenaSeparada[2].toUpperCase().charAt(0);

	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public String toString() {
		return "nombre=" + nombre + ", edad=" + edad + ", sexo=" + sexo;
	}

	public String formatearParaGuardar() {
		return edad + ";" + nombre + ";" + sexo;
	}

}
