package accesoAFicheros;

public class Ejercicio6 {

	public static void main(String[] args) {
		
		// Creacion de objetos
		Ejercicio6Persona p1 = new Ejercicio6Persona(20, "Julio Perez", 'M');
		p1.añadirTrabajo(new Ejercicio6Trabajo(1, "Albanil"));
		p1.añadirTrabajo(new Ejercicio6Trabajo(1, "Encofrador"));
		Ejercicio6Persona p2 = new Ejercicio6Persona(21, "Ana Gutierrez", 'F');
		p2.añadirTrabajo(new Ejercicio6Trabajo(2, "Ingeniero"));
		Ejercicio6Persona p3 = new Ejercicio6Persona(84, "Emilia Rivilla", 'F');
		p3.añadirTrabajo(new Ejercicio6Trabajo(2, "Profesora"));
		p3.añadirTrabajo(new Ejercicio6Trabajo(3, "Locutora de radio"));
		
		

	}

}
