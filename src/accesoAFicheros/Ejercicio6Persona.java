package accesoAFicheros;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Ejercicio6Persona {
	int edad;// >0
	String nombre;
	ArrayList<Ejercicio6Trabajo> trabs;// Trabajos en su vida laboral(0..n)
	char sexo;

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Ejercicio6Trabajo> getTrabs() {
		return trabs;
	}

	public void setTrabs(ArrayList<Ejercicio6Trabajo> trabs) {
		this.trabs = trabs;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public Ejercicio6Persona(int edad, String nombre, char sexo) {
		this.edad = edad;
		this.nombre = nombre;
		this.sexo = sexo;
		this.trabs = new ArrayList<Ejercicio6Trabajo>();
	}

	public String toString() {
		return "Persona [edad=" + edad + ", nombre=" + nombre + ", trabs=" + trabs + ", sexo=" + sexo + "]";
	}

	public void añadirTrabajo(Ejercicio6Trabajo t) {
		trabs.add(t);
	}

	public String formatearParaGuardar() {
		String trabajos = Ejercicio6Trabajo.formatearParaGuardar(trabs);
		return edad + ";" + nombre + ";" + sexo + ";" + trabajos;
	}

	public void guardarEnFichero(File ruta) {

		PrintWriter pw = null;
		DataOutputStream dos = null;
		ObjectOutputStream oos = null;

		try {
			// Escritura con PrintWriter
			pw = new PrintWriter(new FileWriter(new File(ruta + ".txt")));
			pw.println(formatearParaGuardar());

			// Escritura con DataOutputStream
			dos = new DataOutputStream(new FileOutputStream(new File(ruta + ".bin")));
			dos.writeInt(getEdad());
			dos.writeUTF(getNombre());
			dos.writeChar((char) getSexo());

			// Escritura con ObjectOutputStream
			oos = new ObjectOutputStream(new FileOutputStream(new File(ruta + ".obj")));
			oos.writeObject(this);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pw != null)
				pw.close();
			try {
				if (dos != null)
					dos.close();
				if (oos != null)
					oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println(".\n.\n");
		System.out.println("Se ha guardado correctamente los 3 objetos con el nombre: \n" + ruta + ".txt, " + ruta
				+ ".bin, " + ruta + ".obj");

	}

}
