package accesoAFicheros;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

import ejercicio1.soluc_SistemaDeFicheros.Utilidades;

public class Ejercicio3 {

	public static void main(String[] args) {
		File ruta = null;

		// Seleccionar Imagen
		JFileChooser fc = new JFileChooser(".");
		fc.setDialogTitle("Seleccione carpeta donde buscar");
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int seleccion = fc.showOpenDialog(fc);
		if (seleccion == JFileChooser.APPROVE_OPTION) {
			ruta = new File(fc.getSelectedFile().getAbsolutePath());
		}

		DataInputStream dis = null;
		try {
			// Vincular dis con imagen
			dis = new DataInputStream(new FileInputStream(ruta));

			// Se comprueba que sea un imagen jpg
			int numero = 0;
			if (dis.read() == 0xff && dis.read() == 0xd8) {
				do {
					numero = dis.read();
					if (numero == -1) {
						System.out.println("Archivo corrupto");
						return;
					}
				} while (!(numero == 0xff && dis.read() == 0xc0));
				// Se pasan 3 bytes
				dis.read();
				dis.read();
				dis.read();
				System.out.println("Altura:" + dis.readShort());
				System.out.println("Ancho:" + dis.readShort());

			} else {
				// Se notifica que no es un JPEG
				System.out.println("No es una imagen JPEG");
			}

		} catch (FileNotFoundException e) {
			Utilidades.notificaError(null, "Error al leer el archivo", e, null);
		} catch (IOException e) {
			Utilidades.notificaError(null, "Error al leer el archivo", e, null);
			e.printStackTrace();
		} finally {
			try {
				dis.close();
			} catch (IOException e) {
				Utilidades.notificaError(null, "Error al cerrar el archivo", e, null);
				e.printStackTrace();
			}
		}
	}
}
