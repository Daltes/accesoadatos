package hibernateEJE1;

import java.io.File;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Principal {

	static SessionFactory sessionFactory;

	public static void main(String[] args) {
		Configuration configuration = new Configuration();
		configuration.configure(new File("confHibernate/hibernate.cfg.xml"));

		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();

		sessionFactory = configuration.buildSessionFactory(serviceRegistry);

		Session session = sessionFactory.openSession();

		session.beginTransaction();

		Seguro seguro = new Seguro("12345678x", "Jonathan", "Catagña", "Pillasagua", 99, 50, new Date());

		System.out.println("=======================Crear=======================");
		System.out.println(seguro);
		System.out.println("correcto\n");

		System.out.println("=======================Consultar=======================");
		System.out.println(consultar(seguro, session));
		System.out.println("correcto\n");

		System.out.println("=======================Actualizar=======================");
		seguro.setNif("Cambiado");
		consultar(seguro, session);
		System.out.println(seguro);
		System.out.println("correcto\n");

		System.out.println("=======================Borrar=======================");
		//borrar(seguro, session);
		System.out.println("correcto\n");

		System.out.println("=======================Consultar=======================");
		System.out.println(consultar(seguro, session));
		System.out.println("correcto\n");
		
		session.close();

	}

	public static void guardar(Seguro seguro, Session session) {

		session.save(seguro); // <|--- Aqui guardamos el objeto en la base de datos.

		if (!session.getTransaction().wasCommitted())
			session.getTransaction().commit();

	}

	public static Seguro consultar(Seguro seguro, Session session) {
		return (Seguro) session.get(Seguro.class, seguro.getIdSeguro()); // <|--- Aqui buscamos el objeto en la base de datos.
	}

	public static void actualizar(Seguro seguro, Session session) {

		session.update(seguro); // <|--- Aqui actualizamos el objeto en la base de datos.
		if (!session.getTransaction().wasCommitted())
			session.getTransaction().commit();
	}

	public static void borrar(Seguro seguro, Session session) {

		session.delete(seguro); // <|--- Aqui borramos el objeto en la base de datos.
		if (!session.getTransaction().wasCommitted())
			session.getTransaction().commit();

	}

}
