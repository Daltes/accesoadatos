package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;

public class PersistenciaBD implements Persistencia {

	Connection con;

	public PersistenciaBD() {
		super();
	}

	public void conectarDB(String IP, String usu, String pass, String bd) throws Exception {

		Class.forName("com.mysql.jdbc.Driver");

		con = DriverManager.getConnection("jdbc:mysql://" + IP + "/" + bd, usu, pass);

	}

	public void deconectarDB() throws Exception {
		con.close();
	}

	public ArrayList<Persona> listadoPersonas(String tabla, String orderBy) {
		// Se crea el arrayList que contendra el resultado
		ArrayList<Persona> resultadoBusqueda = new ArrayList<>();

		// String que contiene la query
		String busqueda = "";
		if (orderBy != null) {
			busqueda = "Select * from " + tabla + " order By " + orderBy;
		} else {
			busqueda = "Select * from " + tabla;
		}
		try {
			Statement st = con.createStatement();

			ResultSet rs = st.executeQuery(busqueda);

			while (rs.next()) {
				resultadoBusqueda.add(new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),
						rs.getString("email")));
			}

			rs.close();
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultadoBusqueda;
	}

	public void guardarPersona(String tabla, Persona p) throws Exception {

		String sqlAlta = "INSERT INTO " + tabla + " VALUES(?,?,?,?)";

		PreparedStatement psGuardar = con.prepareStatement(sqlAlta);

		psGuardar.setString(1, p.getNombre());
		psGuardar.setString(2, p.getCP());
		psGuardar.setString(3, p.getPais());
		psGuardar.setString(4, p.getEmail());

		try {
			psGuardar.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {

			String sqlModificar = "UPDATE " + tabla + " SET nombre=?, CP=?, pais=? WHERE email=?";

			PreparedStatement psModificar = con.prepareStatement(sqlModificar);

			psModificar.setString(1, p.getNombre());
			psModificar.setString(2, p.getCP());
			psModificar.setString(3, p.getPais());
			psModificar.setString(4, p.getEmail());
			psModificar.executeUpdate();
			psModificar.close();
		}

		psGuardar.close();

	}

	public void borrarPersona(String tabla, String email) throws Exception {

		String sqlBorrar = "DELETE FROM " + tabla + " WHERE email = '" + email + "'";

		Statement st = con.createStatement();
		st.executeUpdate(sqlBorrar);
		st.close();
	}

	public Persona consultarPersona(String tabla, String email) throws Exception {

		String sqlbusqueda = "Select * from " + tabla + " WHERE email = '" + email + "'";

		Statement st = con.createStatement();

		ResultSet rs = st.executeQuery(sqlbusqueda);

		if (rs.next()) {
			Persona p = new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),
					rs.getString("email"));
			rs.close();
			st.close();
			return p;
		} else {
			return null;
		}

	}

}
