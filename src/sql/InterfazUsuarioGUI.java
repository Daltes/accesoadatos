package sql;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

public class InterfazUsuarioGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldCP;
	private JTextField textFieldPais;
	private JTextField textFieldEmail;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private String tabla;
	private String nombreBBDD;
	private String ipServidor;
	private String usuario;
	private String contrasena;
	Persistencia bbdd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {

		conectarBBDD();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(12, 22, 70, 15);
		contentPane.add(lblNombre);

		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(88, 20, 114, 19);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		JLabel lblCp = new JLabel("CP:");
		lblCp.setBounds(12, 49, 70, 15);
		contentPane.add(lblCp);

		textFieldCP = new JTextField();
		textFieldCP.setBounds(88, 47, 114, 19);
		contentPane.add(textFieldCP);
		textFieldCP.setColumns(10);

		JLabel lblPais = new JLabel("Pais:");
		lblPais.setBounds(12, 76, 70, 15);
		contentPane.add(lblPais);

		textFieldPais = new JTextField();
		textFieldPais.setBounds(88, 76, 114, 19);
		contentPane.add(textFieldPais);
		textFieldPais.setColumns(10);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(12, 103, 70, 15);
		contentPane.add(lblEmail);

		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(88, 107, 114, 19);
		contentPane.add(textFieldEmail);
		textFieldEmail.setColumns(10);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Ordenar Por:", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(51, 51, 51)));
		panel.setBounds(277, 12, 159, 169);
		contentPane.add(panel);
		panel.setLayout(null);

		JRadioButton rdbtnNombre = new JRadioButton("Nombre");
		rdbtnNombre.setActionCommand("nombre");
		rdbtnNombre.setBounds(5, 17, 149, 23);
		panel.add(rdbtnNombre);
		buttonGroup.add(rdbtnNombre);

		JRadioButton rdbtnCp = new JRadioButton("CP");
		rdbtnCp.setActionCommand("cp");
		rdbtnCp.setBounds(5, 44, 149, 23);
		panel.add(rdbtnCp);
		buttonGroup.add(rdbtnCp);

		JRadioButton rdbtnPais = new JRadioButton("Pais");
		rdbtnPais.setActionCommand("pais");
		rdbtnPais.setBounds(5, 71, 149, 23);
		panel.add(rdbtnPais);
		buttonGroup.add(rdbtnPais);

		JRadioButton rdbtnEmail = new JRadioButton("Email");
		rdbtnEmail.setActionCommand("email");
		rdbtnEmail.setBounds(5, 98, 149, 23);
		panel.add(rdbtnEmail);
		buttonGroup.add(rdbtnEmail);

		JRadioButton rdbtnSinOrdenar = new JRadioButton("Sin Ordenar");
		rdbtnSinOrdenar.setSelected(true);
		rdbtnSinOrdenar.setBounds(5, 126, 149, 23);
		panel.add(rdbtnSinOrdenar);
		buttonGroup.add(rdbtnSinOrdenar);

		JButton btnAlta = new JButton("Alta");
		btnAlta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Se comprueba si la PK existe
				Persona busqueda = null;
				try {
					busqueda = bbdd.consultarPersona(tabla, textFieldEmail.getText());
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

				if (busqueda != null) {
					JOptionPane.showMessageDialog(null, "La persona ya existe");
					return;
				}

				// Se ccomprueba que todos los datos esten rellenados
				if (textFieldNombre.getText().isEmpty() || textFieldCP.getText().isEmpty()
						|| textFieldEmail.getText().isEmpty() || textFieldPais.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Debe rellenar todos los datos");
					return;
				}

				// Se da de alta
				try {
					bbdd.guardarPersona(tabla, new Persona(textFieldNombre.getText(), textFieldCP.getText(),
							textFieldPais.getText(), textFieldEmail.getText()));
				} catch (Exception e1) {
					notificaError(null, "Alta de persona", e1, "No se ha podido dar de alta a la persona");
					return;
				}

				JOptionPane.showMessageDialog(null, "Se ha dado de alta correctamente");

			}
		});
		btnAlta.setBounds(12, 237, 117, 25);
		contentPane.add(btnAlta);

		JButton btnBaja = new JButton("Baja");
		btnBaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Se comprueba que el campo de email no este vacio
				if (textFieldEmail.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Debe introducir un email para eliminar");
					return;
				}

				// Se comprueba si existe
				Persona busqueda;
				try {
					busqueda = bbdd.consultarPersona(tabla, textFieldEmail.getText());
					// Error si no existe
					if (busqueda == null) {
						JOptionPane.showMessageDialog(null, "No se ha encontrado la persona");
						return;
					}

					// Se pide confirmacion
					if (JOptionPane.showConfirmDialog(null, "¿Estas seguro de eliminar a esa persona?",
							"Listado personas", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
						bbdd.borrarPersona(tabla, textFieldEmail.getText());
					}

				} catch (Exception e1) {
					notificaError(null, "Error al eliminar", e1, "No se ha podido realizar la eliminacion en la bbdd");
				}

			}
		});
		btnBaja.setBounds(141, 237, 117, 25);
		contentPane.add(btnBaja);

		JButton btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Se comprueba que el email no este vacio
				if (textFieldEmail.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Debe introducir un email para modificar");
					return;
				}

				try {
					// Se compreuba que el email existe
					if (bbdd.consultarPersona(tabla, textFieldEmail.getText()) == null) {
						JOptionPane.showMessageDialog(null, "El email que ha introducido no existe");
						return;
					}
				} catch (Exception e1) {
					notificaError(null, "Modificar persona", e1, "\n No se ha podido modificar la persona");
				}
				// Se compreuba que los campos no esten vacios
				if (textFieldNombre.getText().isEmpty() || textFieldCP.getText().isEmpty()
						|| textFieldEmail.getText().isEmpty() || textFieldPais.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Debe rellenar todos los datos");
					return;
				}

				try {
					// Se pide confirmacion
					if (JOptionPane.showConfirmDialog(null, "¿Desea modificar la persona?", "Listado personas",
							JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION) {
						return;
					}

					bbdd.guardarPersona(tabla, new Persona(textFieldNombre.getText(), textFieldCP.getText(),
							textFieldPais.getText(), textFieldEmail.getText()));
				} catch (Exception e1) {
					notificaError(null, "Alta de persona", e1, "No se ha podido dar de alta a la persona");
					return;
				}

				JOptionPane.showMessageDialog(null, "Se ha dado modificado correctamente");

			}
		});
		btnModificar.setBounds(270, 237, 117, 25);
		contentPane.add(btnModificar);

		JButton btnConsultaro = new JButton("Consultar");
		btnConsultaro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (textFieldEmail.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Debe introducir un email para buscar");
					return;
				}

				try {
					Persona busqueda = bbdd.consultarPersona(tabla, textFieldEmail.getText());

					if (busqueda == null) {
						JOptionPane.showMessageDialog(null, "No se ha encontrado la persona");
						return;
					}

					textFieldNombre.setText(busqueda.getNombre());
					textFieldCP.setText(busqueda.getCP());
					textFieldPais.setText(busqueda.getPais());
					textFieldEmail.setText(busqueda.getEmail());

				} catch (Exception e) {
					notificaError(null, "Error al buscar", e, "No se ha podido realizar la busqueda en la bbdd");
				}

			}
		});
		btnConsultaro.setBounds(12, 138, 117, 25);
		contentPane.add(btnConsultaro);

		JButton btnListar = new JButton("Listar");
		btnListar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Persona> listado = null;

				if (rdbtnSinOrdenar.isSelected()) {
					listado = bbdd.listadoPersonas(tabla, null);
				} else {
					listado = bbdd.listadoPersonas(tabla, buttonGroup.getSelection().getActionCommand());
				}
				for (int i = 0; i < listado.size(); i++) {

					textFieldNombre.setText(listado.get(i).getNombre());
					textFieldCP.setText(listado.get(i).getCP());
					textFieldPais.setText(listado.get(i).getPais());
					textFieldEmail.setText(listado.get(i).getEmail());

					if (JOptionPane.showConfirmDialog(null, "Desea continuar", "Listado personas",
							JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION) {
						return;
					}

				}
			}
		});
		btnListar.setBounds(141, 138, 117, 25);
		contentPane.add(btnListar);

		enableEvents(java.awt.AWTEvent.WINDOW_EVENT_MASK);

	}

	@Override
	protected void processWindowEvent(java.awt.event.WindowEvent e) {

		super.processWindowEvent(e);
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			try {
				bbdd.deconectarDB();
				;
			} catch (Exception ex) {
			}
		}
	}

	private void conectarBBDD() {
		// Se conecta con la base de datos
		bbdd = new PersistenciaBD();
		// Se lee el fichero CFG.INI para conectarse a la bbdd
		BufferedReader bf = null;
		try {
			bf = new BufferedReader(new FileReader(new File("CFG.INI")));

			tabla = bf.readLine();
			nombreBBDD = bf.readLine();
			ipServidor = bf.readLine();
			usuario = bf.readLine();
			contrasena = bf.readLine();
		} catch (IOException e) {
			notificaError(this, "Error con fichero CFG.INI", e, "No se ha podido leer el fichero CFG.INI");
		} finally {
			if (bf != null)
				try {
					bf.close();
				} catch (IOException e) {
					notificaError(this, "Error al cerrar el fichero", e, "No se ha podido cerrar el fichero CFG.INI");
				}
		}

		// Se establece la conexion
		try {
			bbdd.conectarDB(ipServidor, usuario, contrasena, nombreBBDD);
		} catch (Exception e) {
			notificaError(this, "Error a conectar", e, "No se ha podido conectar con la base de datos");
		}
	}

	public static void notificaError(JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido = "";
		if (e != null)
			contenido += e.getClass().getName() + "\n" + e.getMessage(); // Nombre de la excepcion y mensaje de la
																			// excep.
		if (mensaje != null)
			contenido += mensaje;
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
	}

}
