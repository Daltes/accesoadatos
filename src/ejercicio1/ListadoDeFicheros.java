package ejercicio1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ejercicio1.soluc_SistemaDeFicheros.Utilidades;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

public class ListadoDeFicheros extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldRutaFichero;
	private JTextField textFieldTamano;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnMayores;
	private JRadioButton rdbtnMenores;
	private JCheckBox chckbxIncluirOcultos;
	private JCheckBox chckbxIncluirSubcarpetas;
	private JButton btnBuscar;
	private JScrollPane scrollPane;
	private JTextPane textPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListadoDeFicheros frame = new ListadoDeFicheros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListadoDeFicheros() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 660, 406);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnSeleccionarFichero = new JButton("Seleccionar Fichero");
		btnSeleccionarFichero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarFichero();
			}
		});
		btnSeleccionarFichero.setBounds(12, 24, 251, 25);
		contentPane.add(btnSeleccionarFichero);

		textFieldRutaFichero = new JTextField();
		textFieldRutaFichero.setEditable(false);
		textFieldRutaFichero.setBounds(275, 27, 362, 19);
		contentPane.add(textFieldRutaFichero);
		textFieldRutaFichero.setColumns(10);

		JLabel lblTamaoEnBytes = new JLabel("Tamaño en Bytes");
		lblTamaoEnBytes.setBounds(22, 72, 141, 25);
		contentPane.add(lblTamaoEnBytes);

		textFieldTamano = new JTextField();
		textFieldTamano.setText("1");
		textFieldTamano.setBounds(161, 75, 102, 19);
		contentPane.add(textFieldTamano);
		textFieldTamano.setColumns(10);

		rdbtnMayores = new JRadioButton("Mayores");
		rdbtnMayores.setSelected(true);
		buttonGroup.add(rdbtnMayores);
		rdbtnMayores.setBounds(275, 73, 102, 23);
		contentPane.add(rdbtnMayores);

		rdbtnMenores = new JRadioButton("Menores");
		buttonGroup.add(rdbtnMenores);
		rdbtnMenores.setBounds(275, 102, 102, 23);
		contentPane.add(rdbtnMenores);

		chckbxIncluirOcultos = new JCheckBox("Incluir Ocultos");
		chckbxIncluirOcultos.setBounds(381, 73, 129, 23);
		contentPane.add(chckbxIncluirOcultos);

		chckbxIncluirSubcarpetas = new JCheckBox("Incluir Subcarpetas");
		chckbxIncluirSubcarpetas.setBounds(381, 102, 179, 23);
		contentPane.add(chckbxIncluirSubcarpetas);

		btnBuscar = new JButton("BUSCAR");
		btnBuscar.setEnabled(false);
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				realizarBusqueda();
			}
		});
		btnBuscar.setBounds(520, 72, 117, 25);
		contentPane.add(btnBuscar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(44, 178, 593, 167);
		contentPane.add(scrollPane);

		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);

		JLabel lblResultadoDeLa = new JLabel("Resultado de la búsqueda");
		lblResultadoDeLa.setBounds(44, 151, 219, 15);
		contentPane.add(lblResultadoDeLa);
	}

	protected void realizarBusqueda() {
		// Comprueba el tamaño de bytes
		long tamano = 0;
		try {
			tamano = Long.valueOf(textFieldTamano.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(contentPane, "No se ha introducido un numero \n" + e.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		// Establece el criterio de busqueda
		char criterio = '0';
		if (rdbtnMayores.isSelected()) {
			criterio = '+';
		} else {
			criterio = '-';
		}

		if (tamano < 0) {
			Utilidades.notificaError(this, "Error en tamaño", null, "No se admiten numeros negativos");
			return;
		}

		ArrayList<File> listaFicheros = buscarArchivosPorTamanio(new File(textFieldRutaFichero.getText()), tamano,
				criterio, chckbxIncluirOcultos.isSelected(), chckbxIncluirSubcarpetas.isSelected());

		// StyledDocument doc = textPane.getStyledDocument();
		// Style style = textPane.addStyle("soy un estilo", null);
		// StyleConstants.setForeground(style, Color.BLACK);
		//
		// for (int i = 0; i < listaFicheros.size(); i++) {
		//
		// try {
		// doc.insertString(doc.getLength(),
		// listaFicheros.get(i).getCanonicalPath() + "(" + listaFicheros.get(i).length()
		// + " bytes)\n",
		// style);
		// } catch (BadLocationException e1) {
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// }

		String resultado = "";
		for (int i = 0; i < listaFicheros.size(); i++) {
			try {
				resultado += listaFicheros.get(i).getCanonicalPath() + "\n";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		textPane.setText(resultado);

		JOptionPane.showMessageDialog(contentPane, "Se han encontrado " + listaFicheros.size() + " ficheros.");
		resultado = "";
	}

	public static ArrayList<File> buscarArchivosPorTamanio(File carpetaDondeBuscar, long tamEnBytes, char criterio,
			boolean incluirOcultos, boolean incluirSubcarpetas) {
		if (carpetaDondeBuscar == null)
			throw new IllegalArgumentException("El primer argumento debe ser un objeto de la clase File");
		if (tamEnBytes < 0)
			throw new IllegalArgumentException("No se admiten numeros negativos");
		if (!carpetaDondeBuscar.isDirectory())
			throw new IllegalArgumentException("El objeto File no es una carpeta");
		if (!(criterio == '+' || criterio == '-'))
			throw new IllegalArgumentException("El parametro criterio debe ser un '+' o '-'");

		File[] listadoCompletoFicheros = carpetaDondeBuscar.listFiles();
		ArrayList<File> listadoFicheros = new ArrayList<File>();
			for (int i = 0; i < listadoCompletoFicheros.length; i++) {
				if (criterio == '+') {
					if (incluirOcultos) {
						if (listadoCompletoFicheros[i].length() >= tamEnBytes) {

							if (listadoCompletoFicheros[i].isFile()) {
								listadoFicheros.add(listadoCompletoFicheros[i]);
							} else {
								if (incluirSubcarpetas)
									listadoFicheros.addAll(buscarArchivosPorTamanio(listadoCompletoFicheros[i],
											tamEnBytes, criterio, incluirOcultos, incluirSubcarpetas));
							}
						}

					} else {
						if (!listadoCompletoFicheros[i].isHidden()) {

							if (listadoCompletoFicheros[i].length() >= tamEnBytes) {

								if (listadoCompletoFicheros[i].isFile()) {
									listadoFicheros.add(listadoCompletoFicheros[i]);
								} else {
									if (incluirSubcarpetas)
										listadoFicheros.addAll(buscarArchivosPorTamanio(listadoCompletoFicheros[i],
												tamEnBytes, criterio, incluirOcultos, incluirSubcarpetas));
								}

							}
						}

					}
				} else {
					if (incluirOcultos) {
						if (listadoCompletoFicheros[i].length() <= tamEnBytes) {

							if (listadoCompletoFicheros[i].isFile()) {
								listadoFicheros.add(listadoCompletoFicheros[i]);
							}

							if (incluirSubcarpetas && listadoCompletoFicheros[i].isDirectory()) {
								if (incluirSubcarpetas)
									listadoFicheros.addAll(buscarArchivosPorTamanio(listadoCompletoFicheros[i],
											tamEnBytes, criterio, incluirOcultos, incluirSubcarpetas));
							}

						}

					} else {

						if (listadoCompletoFicheros[i].length() <= tamEnBytes) {

							if (listadoCompletoFicheros[i].isFile()) {
								listadoFicheros.add(listadoCompletoFicheros[i]);
							}

							if (incluirSubcarpetas && listadoCompletoFicheros[i].isDirectory()) {
								if (incluirSubcarpetas)
									listadoFicheros.addAll(buscarArchivosPorTamanio(listadoCompletoFicheros[i],
											tamEnBytes, criterio, incluirOcultos, incluirSubcarpetas));
							}
						}

					}
				}
			}
		return listadoFicheros;
	}

	private void seleccionarFichero() {
		JFileChooser fc = new JFileChooser("/home/alumno/prueba");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.setDialogTitle("Seleccionar Carpeta");
		int option = fc.showOpenDialog(contentPane);

		if (option == JFileChooser.APPROVE_OPTION) {
			
			if (!fc.getSelectedFile().isDirectory() || !fc.getSelectedFile().exists()) {
				Utilidades.notificaError(this, "Error", null, "No es un directorio correcto");
				return;
			}
			
			try {
				textFieldRutaFichero.setText(fc.getSelectedFile().getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			btnBuscar.setEnabled(true);
		}

	}
}
