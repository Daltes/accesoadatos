package ejercicio1.soluc_SistemaDeFicheros;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Utilidades {
	public static ArrayList<File> buscarArchivosPorTamanio(File carpetaDondeBuscar, long tamEnBytes, char criterio,
			boolean incluirOcultos, boolean incluirSubcarpetas) {
		if (carpetaDondeBuscar == null)
			throw new IllegalArgumentException("El primer argumento debe ser un objeto de la clase File");
		if (!carpetaDondeBuscar.isDirectory())
			throw new IllegalArgumentException("El objeto File no es una carpeta");
		if (tamEnBytes < 0L)
			throw new IllegalArgumentException("El numero tiene que ser >=0");
		boolean mayores = true;
		switch (criterio) {
		case '+':
			mayores = true;
			break;
		case '-':
			mayores = false;
			break;
		default:
			throw new IllegalArgumentException("El argumento criterio debe ser un caracter '+' o '-'");
		}

		ArrayList<File> al = new ArrayList<File>();

		File[] lista = carpetaDondeBuscar.listFiles();
		if (lista == null) // Error al listar carpeta: falta de permisos, etc
			return al;

		for (int i = 0; i < lista.length; i++) {
			boolean incluir = !lista[i].isHidden() || lista[i].isHidden() && incluirOcultos;// Ignoramos carpetas y
																							// ficheros ocultos
			if (incluir) {
				if (lista[i].isFile()) {
					if (mayores && lista[i].length() >= tamEnBytes || !mayores && lista[i].length() <= tamEnBytes)
						al.add(lista[i]);
				} else {// Subcarpeta
					if (incluirSubcarpetas) {
						// Añadimos los ficheros que se encuentren en subcarpetas mediante recursividad
						ArrayList<File> al2 = buscarArchivosPorTamanio(lista[i], tamEnBytes, criterio, incluirOcultos,
								incluirSubcarpetas);
						if (al2 != null)
							al.addAll(al2);
					}
				}
			}
		}
		return al;
	}

	public static String mostrarInfoFileTam(File f) {
		return f.getAbsolutePath() + " (" + f.length() + " bytes)";
	}

	public static void notificaError(JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido = "";
		if (e != null)
			contenido += e.getClass().getName() + "\n" + e.getMessage(); // Nombre de la excepcion y mensaje de la
																			// excep.
		if (mensaje != null)
			contenido += mensaje;
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
	}

	public boolean preguntaUsuario(JFrame padre, String titulo, String mensaje) {
		/// Mensaje de confirmacion. Devuelve true si el usuario pulsa SI
		return JOptionPane.showConfirmDialog(padre, mensaje, titulo,
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}

	public static boolean esFormatoDeFechaCorrecto(String formato, String fecha) {
		// Se comprueba que los parametros no sean nulos
		if (formato == null) {
			throw new IllegalArgumentException("Parametro formato es nulo");
		}

		if (fecha == null) {
			throw new IllegalArgumentException("Parametro fecha es nulo");
		}

		// Se realiza un transpaso del formato a un Long, si falla es que la fecha o el
		// formato es incorrecto y devuelve un false
		try {
			new SimpleDateFormat(formato).parse(fecha);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public static long pasarFechaALong(String formato, String fecha) {
		// Comprueba si el formato y la fecha son correctas, si no lo son lanza una
		// excepcion
		if (!esFormatoDeFechaCorrecto(formato, fecha))
			throw new IllegalArgumentException("El formato o la fecha es incorrecto");

		long fechaLong;
		try {
			fechaLong = new SimpleDateFormat(formato).parse(fecha).getTime();
		} catch (ParseException e) {
			throw new IllegalArgumentException("El formato o la fecha es incorrecto");
		}

		return fechaLong;
	}

	public static ArrayList<File> buscarArchivosPorFechModi(File carpetaDondeBuscar, long fecha, char criterio,
			boolean incluirOcultos, boolean incluirSubcarpetas) {
		if (carpetaDondeBuscar == null)
			throw new IllegalArgumentException("El primer argumento debe ser un objeto de la clase File");
		if (!carpetaDondeBuscar.isDirectory())
			throw new IllegalArgumentException("El objeto File no es una carpeta");
		boolean mayores = true;
		switch (criterio) {
		case '+':
			mayores = true;
			break;
		case '-':
			mayores = false;
			break;
		default:
			throw new IllegalArgumentException("El argumento criterio debe ser un caracter '+' o '-'");
		}

		ArrayList<File> al = new ArrayList<File>();

		File[] lista = carpetaDondeBuscar.listFiles();
		if (lista == null) // Error al listar carpeta: falta de permisos, etc
			return null;

		for (int i = 0; i < lista.length; i++) {
			boolean incluir = !lista[i].isHidden() || lista[i].isHidden() && incluirOcultos;// Ignoramos carpetas y
																							// ficheros ocultos
			if (incluir) {
				if (lista[i].isFile()) {
					if (mayores && lista[i].lastModified() >= fecha || !mayores && lista[i].lastModified() <= fecha)
						al.add(lista[i]);
				} else {// Subcarpeta
					if (incluirSubcarpetas) {
						// Añadimos los ficheros que se encuentren en subcarpetas mediante recursividad
						ArrayList<File> al2 = buscarArchivosPorFechModi(lista[i], fecha, criterio, incluirOcultos,
								incluirSubcarpetas);
						if (al2 != null)
							al.addAll(al2);
					}
				}
			}
		}
		return al;
	}

	public static ArrayList<File> buscarArchivosPorNombre(File carpetaDondeBuscar, String patronNombre,
			boolean incluirOcultos, boolean incluirSubcarpetas) {
		if (carpetaDondeBuscar == null)
			throw new IllegalArgumentException("El primer argumento debe ser un objeto de la clase File");
		if (!carpetaDondeBuscar.isDirectory())
			throw new IllegalArgumentException("El objeto File no es una carpeta");
		if(patronNombre.isEmpty())
			throw new IllegalArgumentException("No se ha introducido un patron");

		ArrayList<File> al = new ArrayList<File>();

		File[] lista = carpetaDondeBuscar.listFiles();
		if (lista == null) // Error al listar carpeta: falta de permisos, etc
			return null;

		for (int i = 0; i < lista.length; i++) {
			boolean incluir = !lista[i].isHidden() || lista[i].isHidden() && incluirOcultos;// Ignoramos carpetas y
																							// ficheros ocultos
			if (incluir) {
				if (lista[i].isFile()) {
					if (lista[i].getName().matches(patronNombre))
						al.add(lista[i]);
				} else {// Subcarpeta
					if (incluirSubcarpetas) {
						// Añadimos los ficheros que se encuentren en subcarpetas mediante recursividad
						ArrayList<File> al2 = buscarArchivosPorNombre(lista[i], patronNombre, incluirOcultos,
								incluirSubcarpetas);
						if (al2 != null)
							al.addAll(al2);
					}
				}
			}
		}
		return al;
	}

}
