package ejercicio1.soluc_SistemaDeFicheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class BuscarPorTamanoConsola {

	public static void main(String[] args) {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		File ruta = null;
		long tamano = 0;
		char criterio;
		boolean incluirOcultos = false;
		boolean incluirSubcarpetas = false;
		boolean bien;
		String criterioTexto = "";
		
		ArrayList<File> resultado = new ArrayList<File>();
		
		// Lectura de la ruta
		do {
			bien = false;
			System.out.println("Introduzca la ruta de la carpeta:");
			try {
				ruta = new File(br.readLine().trim());
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (!ruta.exists() || !ruta.isDirectory()) {
				bien = !bien;
				System.out.println("Error al seleccionar la carpeta");
			}
		} while (bien);

		// Lectura de tamaño para filtrar
		do {
			bien = false;
			System.out.println("Introduzca el tamaño por el que quieres filtrar:");
			try {
				tamano = Long.parseLong(br.readLine().trim());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				System.out.println("Introduzca un numero correcto");
				bien = true;
			}

			if (!(tamano >= 0)) {
				System.out.println("Introduzca un numero mayor a 0");
				bien = true;
			}

		} while (bien);

		// Lectura del criterio
		do {
			bien = false;
			criterioTexto = null;
			System.out.println("Introduzca + para buscar archivos mayores o - para menores");
			try {
				criterioTexto = br.readLine().trim();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (criterioTexto.length() != 1) {
				System.out.println("Introduzca solo un caracter");
				bien = true;
			}

			if (!(criterioTexto.equals("+") || criterioTexto.equals("-"))) {
				System.out.println("Introduzca un + o -");
				bien = true;
			}

			if (!(tamano >= 0)) {
				System.out.println("Introduzca un numero mayor a 0");
				bien = true;
			}

		} while (bien);
		criterio = criterioTexto.charAt(0);

		// Incluir ocultos
		String respuesta = null;
		do {
			bien = false;
			System.out.println("¿Desea incluir ocultos (si o no)?");
			try {
				respuesta = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			respuesta = respuesta.trim().toLowerCase();
			if (!(respuesta.equals("si") || respuesta.equals("no"))) {
				bien = true;
				System.out.println("Introduzca si o no");
			} else {
				if (respuesta.equals("si"))
					incluirOcultos = true;
				else
					incluirOcultos = false;
			}
		} while (bien);
		
		//Incluir subcarpetas
		do {
			bien = false;
			System.out.println("¿Desea incluir subcarpetas (si o no)?");
			try {
				respuesta = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			respuesta = respuesta.trim().toLowerCase();
			if (!(respuesta.equals("si") || respuesta.equals("no"))) {
				bien = true;
				System.out.println("Introduzca si o no");
			} else {
				if (respuesta.equals("si"))
					incluirSubcarpetas = true;
				else
					incluirSubcarpetas = false;
			}
		} while (bien);

		resultado = Utilidades.buscarArchivosPorTamanio(ruta, tamano, criterio, incluirOcultos, incluirSubcarpetas);
		String resultadoFinal = "";
		
		if (resultado!=null){
			for (int i = 0; i < resultado.size(); i++) {
				try {
					resultadoFinal += resultado.get(i).getCanonicalPath()+"\n";
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println(resultadoFinal);
			System.out.println("Se han encontrado "+resultado.size()+" archivos");
		}
		
	}

}
