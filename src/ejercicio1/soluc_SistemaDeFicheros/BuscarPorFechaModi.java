package ejercicio1.soluc_SistemaDeFicheros;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

public class BuscarPorFechaModi extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField ruta;
	private JTextField fecha;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnMayores;
	private JTextArea textAreaRes;
	private JCheckBox chckbxIncluirOcultos;
	private JScrollPane scrollPane;
	private JFrame ventanaGUI = this;
	private JCheckBox chckbxIncluirSubcarpetas;
	private JButton btnBuscar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscarPorFechaModi frame = new BuscarPorFechaModi();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscarPorFechaModi() {
		setTitle("Buscar por tama\u00F1o");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 682, 485);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Seleccionar Carpeta");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonSeleccionar();
			}
		});
		btnNewButton.setBounds(10, 24, 194, 23);
		contentPane.add(btnNewButton);

		ruta = new JTextField();
		ruta.setEditable(false);
		ruta.setBounds(214, 25, 298, 20);
		contentPane.add(ruta);
		ruta.setColumns(10);

		JLabel lblFecha = new JLabel("DD-MM-AAAA");
		lblFecha.setFont(new Font("Dialog", Font.BOLD, 12));
		lblFecha.setBounds(25, 76, 125, 14);
		contentPane.add(lblFecha);

		fecha = new JTextField();
		fecha.setText("0");
		fecha.setBounds(153, 74, 86, 20);
		contentPane.add(fecha);
		fecha.setColumns(10);

		rdbtnMayores = new JRadioButton("Antes de");
		rdbtnMayores.setSelected(true);
		buttonGroup.add(rdbtnMayores);
		rdbtnMayores.setBounds(278, 72, 86, 23);
		contentPane.add(rdbtnMayores);

		JRadioButton rdbtnMenores = new JRadioButton("Despues de");
		buttonGroup.add(rdbtnMenores);
		rdbtnMenores.setBounds(278, 102, 109, 23);
		contentPane.add(rdbtnMenores);

		chckbxIncluirOcultos = new JCheckBox("Incluir Ocultos");
		chckbxIncluirOcultos.setFont(new Font("Dialog", Font.BOLD, 12));
		chckbxIncluirOcultos.setBounds(403, 72, 136, 23);
		contentPane.add(chckbxIncluirOcultos);

		btnBuscar = new JButton("BUSCAR");
		btnBuscar.setEnabled(false);
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonBuscar();
			}
		});
		btnBuscar.setBounds(547, 76, 108, 33);
		contentPane.add(btnBuscar);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 170, 630, 266);
		contentPane.add(scrollPane);

		textAreaRes = new JTextArea();
		scrollPane.setViewportView(textAreaRes);

		JLabel lblResultadosDeLa = new JLabel("Resultados de la b\u00FAsqueda");
		lblResultadosDeLa.setBounds(25, 145, 244, 14);
		contentPane.add(lblResultadosDeLa);

		chckbxIncluirSubcarpetas = new JCheckBox("Incluir Subcarpetas");
		chckbxIncluirSubcarpetas.setFont(new Font("Dialog", Font.BOLD, 12));
		chckbxIncluirSubcarpetas.setBounds(403, 116, 176, 23);
		contentPane.add(chckbxIncluirSubcarpetas);
	}

	protected void botonSeleccionar() {
		JFileChooser fc = new JFileChooser(".");
		fc.setDialogTitle("Seleccione carpeta donde buscar");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int seleccion = fc.showOpenDialog(ventanaGUI);
		if (seleccion == JFileChooser.APPROVE_OPTION) {

			if (!fc.getSelectedFile().isDirectory() || !fc.getSelectedFile().exists()) {
				Utilidades.notificaError(this, "Error", null, "No es un directorio correcto");
				return;
			}

			ruta.setText(fc.getSelectedFile().getAbsolutePath());
			btnBuscar.setEnabled(true);
		}
	}

	protected void botonBuscar() {
		textAreaRes.setText("");

		if (!Utilidades.esFormatoDeFechaCorrecto("dd/MM/yyyy", fecha.getText())) {
			Utilidades.notificaError(this, "Error en Fecha", null, "La fecha es incorrecta");
			return;
		}

		ArrayList<File> res = Utilidades.buscarArchivosPorTamanio(new File(ruta.getText()),
				Utilidades.pasarFechaALong("dd/MM/yyyy", fecha.getText()), rdbtnMayores.isSelected() ? '+' : '-',
				chckbxIncluirOcultos.isSelected(), chckbxIncluirSubcarpetas.isSelected());
		if (res != null) {
			for (int i = 0; i < res.size(); i++) {
				textAreaRes.setText(textAreaRes.getText() + Utilidades.mostrarInfoFileTam(res.get(i)) + "\n");
			}
			JOptionPane.showMessageDialog(ventanaGUI, res.size() + " archivos encontrados.");
		}
	}
}
