package ejemplo01;

import java.io.File;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Principal {
	static SessionFactory sessionFactory;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration();
		configuration.configure(new File("hibernate.cfg.xml"));

		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();

		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
		/*
		Profesor profesor=new Profesor(101, "Juan", "Perez", "García");  //Creamos el objeto
		 
		
		session.beginTransaction();
		 
		session.save(profesor); //<|--- Aqui guardamos el objeto en la base de datos.
		 
		session.getTransaction().commit();
		
		*/
		Session session = sessionFactory.openSession();
		Profesor profesor=(Profesor)session.get(Profesor.class,101);
		System.out.println(profesor);
		
		profesor.setApe1("Perez");
		
		session.beginTransaction();
		 
		session.update(profesor);
		 
		session.getTransaction().commit();
		
		Profesor profesor1=(Profesor)session.get(Profesor.class,101);
		System.out.println(profesor1);
		
		session.close();
	}

}
