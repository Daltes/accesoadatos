package sql3;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GestorBBDD {

	private Connection con;
	private DatabaseMetaData metaDatos;
	private int tipoConexion;
	public static final int CONEXION_DERBY = 1;
	public static final int CONEXION_ACCESS = 2;

	public int getTipoConexion() {
		return tipoConexion;
	}

	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}

	public DatabaseMetaData getMetaDatos() {
		return metaDatos;
	}

	public void setMetaDatos(DatabaseMetaData metaDatos) {
		this.metaDatos = metaDatos;
	}

	public void conectarDB(String IP, String usu, String pass) throws Exception {

		Class.forName("com.mysql.jdbc.Driver");

		con = DriverManager.getConnection("jdbc:mysql://" + IP + "?serverTimezone=UTC", usu, pass);

		metaDatos = con.getMetaData();
	}

	public void conectarDB(File ruta, int tipoConexion) throws Exception {
		this.tipoConexion = tipoConexion;
		switch (tipoConexion) {
		case 1:
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			con = DriverManager.getConnection("jdbc:derby:" + ruta.getAbsolutePath() + ";create=true");
			break;
		case 2:
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			con = DriverManager
					.getConnection("jdbc:ucanaccess://" + ruta.getAbsolutePath() + ";newdatabaseversion=V2010");
			break;
		default:
			throw new IllegalArgumentException("Introduzca un tipo de conexión válido");
		}

		metaDatos = con.getMetaData();

	}

	public void deconectarDB() throws Exception {
		con.close();
	}

	public ArrayList<String> listarBases() throws SQLException {

		ArrayList<String> resultado = new ArrayList<>();

		if (tipoConexion == CONEXION_ACCESS) {
			return resultado;
		}
		ResultSet bbdd;
		if (tipoConexion == CONEXION_DERBY) {
			bbdd = metaDatos.getSchemas();
		} else {
			bbdd = metaDatos.getCatalogs();
		}

		while (bbdd.next()) {
			if (tipoConexion == CONEXION_DERBY) {
				resultado.add(bbdd.getString("TABLE_SCHEM"));
			} else {
				resultado.add(bbdd.getString("TABLE_CAT"));
			}
		}
		bbdd.close();
		return resultado;
	}

	public ArrayList<String> listarTablas(String base) throws SQLException {

		ArrayList<String> resultado = new ArrayList<>();

		ResultSet bbdd;
		if (tipoConexion == CONEXION_DERBY) {
			bbdd = metaDatos.getTables(null, base.toUpperCase(), "%", null);
		} else {
			bbdd = metaDatos.getTables(base, null, "%", null);
		}

		while (bbdd.next()) {
			resultado.add(bbdd.getString("TABLE_NAME"));
		}
		bbdd.close();
		return resultado;
	}

	public ArrayList<Columna> listarColumnas(String base, String tabla) throws SQLException {

		ArrayList<Columna> resultado = new ArrayList<>();

		ResultSet bbdd = metaDatos.getColumns(base, null, tabla, "%");

		while (bbdd.next()) {
			String tipo = "VARCHAR";

			if (bbdd.getString("TYPE_NAME").equals("INTEGER")) {
				tipo = "INTEGER";
			}

			resultado.add(new Columna(bbdd.getString("COLUMN_NAME"), tipo));
		}
		bbdd.close();
		return resultado;
	}

	boolean existeTabla(String base, String tabla) throws SQLException {

		if (tipoConexion == CONEXION_DERBY) {
			tabla = tabla.toUpperCase();
		}

		ResultSet bbdd = metaDatos.getTables(base, null, tabla, null);

		if (bbdd.next()) {
			bbdd.close();
			return true;
		}
		bbdd.close();
		return false;
	}

	boolean existeBD(String base) throws SQLException {

		return listarBases().contains(base.toLowerCase().trim());
	}

	public ArrayList<String> listarTodo(String nombreBaseDatos, String nombreTabla, String delimitador)
			throws Exception {



		ArrayList<String> resultado = new ArrayList<>();

		ArrayList<Columna> columnas = listarColumnas(nombreBaseDatos, nombreTabla);

		if (nombreBaseDatos == null) {
			nombreBaseDatos = "";
		} else {
			nombreBaseDatos += ".";
		}
		
		String sqlConsulta = "SELECT * FROM " + nombreBaseDatos + nombreTabla;

		Statement st = con.createStatement();

		ResultSet rs = st.executeQuery(sqlConsulta);

		while (rs.next()) {
			String resultadoBusqueda = "";

			for (int i = 0; i < columnas.size(); i++) {
				if (i != columnas.size() - 1) {
					resultadoBusqueda += rs.getString(columnas.get(i).getNombre()) + delimitador;
				} else {
					resultadoBusqueda += rs.getString(columnas.get(i).getNombre());
				}
			}
			resultado.add(resultadoBusqueda);
		}

		st.close();
		rs.close();
		return resultado;
	}

	public void ejecutarVariasSentenciasSQL(ArrayList<String> querys) throws Exception {

		Statement st = con.createStatement();

		for (int i = 0; i < querys.size(); i++) {
			st.executeUpdate(querys.get(i));
		}

		st.close();
	}

}
