package sql3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;

public class InterfazUsuarioGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String NOMBRE_TABLA_POR_DEFECTO = "BaseDatosAccess";
	private JPanel contentPane;
	GestorBBDD bbdd;
	private JButton btnDesconectar;
	private JComboBox<String> comboBox_BaseDatos;
	private JList list_Columnas;
	private JComboBox<String> comboBox_Tablas;
	private JScrollPane scrollPane;
	private JTextField textFieldUsuario;
	private JTextField textFieldIP;
	private JTextField textFieldContra;
	private JButton btnImportar;
	private JButton btnExportar;
	private JLabel lblBaseDatos;
	private JLabel lblTabkas;
	private JTextField textFieldRuta;
	private JLabel lblRuta;
	private JButton btnExaminar;
	private File rutaCon;
	private File ruta;
	private JTextField textFieldRutaCon;
	private JComboBox<Object> comboBoxCon;
	private JButton buttonExaminarConnect;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 642, 564);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnConectar = new JButton("Conectar");
		btnConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (comboBoxCon.getSelectedIndex() != 0 && textFieldRutaCon.getText().isEmpty()) {
					notificaError(null, "Ruta de conexion", null, "Debe introducir una ruta");
					return;
				}

				try {
					conectarBBDD();
				} catch (Exception e1) {
					notificaError(null, "Error a conectar", e1, "No se ha podido conectar con la base de datos");
					return;
				}
				comboBoxCon.setEnabled(false);
				buttonExaminarConnect.setEnabled(false);
				comboBox_BaseDatos.removeAllItems();
				comboBox_Tablas.removeAllItems();
				list_Columnas.removeAll();
				btnConectar.setEnabled(false);
				btnDesconectar.setEnabled(true);
				comboBox_BaseDatos.setEnabled(true);
				btnImportar.setEnabled(true);
				textFieldContra.setEnabled(false);
				textFieldUsuario.setEnabled(false);
				textFieldIP.setEnabled(false);

				rellenarComboBox();

				if (comboBoxCon.getSelectedIndex() == 2) {
					rellenarTablas();
				}
			}
		});
		btnConectar.setBounds(461, 149, 117, 25);
		contentPane.add(btnConectar);

		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					bbdd.deconectarDB();
				} catch (Exception e1) {
					notificaError(null, "Error al desconectar", e1, "Se ha producido un error al desconectar la bbdd");
				}

				btnConectar.setEnabled(true);
				btnDesconectar.setEnabled(false);
				comboBox_BaseDatos.setEnabled(false);
				comboBox_Tablas.setEnabled(false);
				btnExportar.setEnabled(false);
				btnImportar.setEnabled(false);
				textFieldContra.setEnabled(true);
				textFieldUsuario.setEnabled(true);
				textFieldIP.setEnabled(true);
				textFieldRuta.setText("");
				comboBoxCon.setEnabled(true);
			}
		});
		btnDesconectar.setEnabled(false);
		btnDesconectar.setBounds(174, 462, 148, 25);
		contentPane.add(btnDesconectar);

		comboBox_BaseDatos = new JComboBox();
		comboBox_BaseDatos.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				rellenarTablas();
			}
		});
		comboBox_BaseDatos.setEnabled(false);
		comboBox_BaseDatos.setBounds(10, 207, 148, 24);
		contentPane.add(comboBox_BaseDatos);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 308, 585, 126);
		contentPane.add(scrollPane);

		list_Columnas = new JList();
		scrollPane.setViewportView(list_Columnas);

		comboBox_Tablas = new JComboBox();
		comboBox_Tablas.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				rellenarColumnas();
			}
		});
		comboBox_Tablas.setEnabled(false);
		comboBox_Tablas.setBounds(254, 207, 148, 25);
		contentPane.add(comboBox_Tablas);

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(10, 136, 70, 15);
		contentPane.add(lblUsuario);

		JLabel lblContrasea = new JLabel("Contraseña");
		lblContrasea.setBounds(154, 136, 93, 15);
		contentPane.add(lblContrasea);

		JLabel lblIp = new JLabel("IP");
		lblIp.setBounds(301, 136, 70, 15);
		contentPane.add(lblIp);

		textFieldUsuario = new JTextField();
		textFieldUsuario.setText("root");
		textFieldUsuario.setBounds(10, 152, 114, 19);
		contentPane.add(textFieldUsuario);
		textFieldUsuario.setColumns(10);

		textFieldIP = new JTextField();
		textFieldIP.setText("127.0.0.1");
		textFieldIP.setBounds(301, 152, 114, 19);
		contentPane.add(textFieldIP);
		textFieldIP.setColumns(10);

		btnExportar = new JButton("Exportar");
		btnExportar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exportarBBDD();
			}
		});
		btnExportar.setEnabled(false);
		btnExportar.setBounds(497, 462, 117, 25);
		contentPane.add(btnExportar);

		btnImportar = new JButton("Importar");
		btnImportar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Importar dialog = new Importar(bbdd);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);

			}
		});
		btnImportar.setEnabled(false);
		btnImportar.setBounds(368, 462, 117, 25);
		contentPane.add(btnImportar);

		textFieldContra = new JTextField();
		textFieldContra.setBounds(154, 152, 114, 19);
		contentPane.add(textFieldContra);
		textFieldContra.setColumns(10);

		lblBaseDatos = new JLabel("Base Datos");
		lblBaseDatos.setBounds(10, 190, 117, 15);
		contentPane.add(lblBaseDatos);

		lblTabkas = new JLabel("Tablas");
		lblTabkas.setBounds(254, 190, 70, 15);
		contentPane.add(lblTabkas);

		textFieldRuta = new JTextField();
		textFieldRuta.setEditable(false);
		textFieldRuta.setBounds(10, 271, 392, 19);
		contentPane.add(textFieldRuta);
		textFieldRuta.setColumns(10);

		lblRuta = new JLabel("Ruta");
		lblRuta.setBounds(10, 243, 70, 15);
		contentPane.add(lblRuta);

		btnExaminar = new JButton("Examinar");
		btnExaminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser(".");
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fc.setDialogTitle("Seleccione una ruta");
				int option = fc.showOpenDialog(null);
				if (option == JFileChooser.APPROVE_OPTION) {
					ruta = fc.getSelectedFile();
					textFieldRuta.setText(ruta.getAbsolutePath());
					btnExportar.setEnabled(true);
				}
			}
		});
		btnExaminar.setBounds(414, 271, 117, 25);
		contentPane.add(btnExaminar);

		JLabel lblTipoDeConexin = new JLabel("Tipo de conexión");
		lblTipoDeConexin.setBounds(10, 11, 134, 14);
		contentPane.add(lblTipoDeConexin);

		comboBoxCon = new JComboBox<Object>();
		comboBoxCon.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {

				if (comboBoxCon.getSelectedIndex() == 0) {
					textFieldContra.setEnabled(true);
					textFieldIP.setEnabled(true);
					textFieldUsuario.setEnabled(true);
					buttonExaminarConnect.setEnabled(false);
				} else {
					textFieldContra.setEnabled(false);
					textFieldIP.setEnabled(false);
					textFieldUsuario.setEnabled(false);
					buttonExaminarConnect.setEnabled(true);
				}

			}
		});
		comboBoxCon.setModel(new DefaultComboBoxModel<Object>(new String[] { "MySQL", "Derby", "Access" }));
		comboBoxCon.setBounds(10, 36, 114, 20);
		contentPane.add(comboBoxCon);

		JLabel lblRuta_1 = new JLabel("Ruta para Derby/Access");
		lblRuta_1.setBounds(10, 72, 171, 14);
		contentPane.add(lblRuta_1);

		textFieldRutaCon = new JTextField();
		textFieldRutaCon.setEditable(false);
		textFieldRutaCon.setBounds(10, 94, 392, 20);
		contentPane.add(textFieldRutaCon);
		textFieldRutaCon.setColumns(10);

		buttonExaminarConnect = new JButton("Examinar");
		buttonExaminarConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFileChooser fc = new JFileChooser(".");

				if (comboBoxCon.getSelectedIndex() == 1) {
					fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				} else {

					fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				}
				fc.setDialogTitle("Seleccione una ruta");
				int option = fc.showOpenDialog(null);
				if (option == JFileChooser.APPROVE_OPTION) {
					rutaCon = fc.getSelectedFile();
					textFieldRutaCon.setText(rutaCon.getAbsolutePath());
				}

			}
		});
		buttonExaminarConnect.setEnabled(false);
		buttonExaminarConnect.setBounds(414, 93, 148, 25);
		contentPane.add(buttonExaminarConnect);
	}

	protected void exportarBBDD() {
		ExportarImportarBBDD exportar = new ExportarImportarBBDD(ruta);
		String nombreBBDD = (String) comboBox_BaseDatos.getSelectedItem();
		if (comboBoxCon.getSelectedIndex() == 2) {
			nombreBBDD = NOMBRE_TABLA_POR_DEFECTO;
		}

		try {
			exportar.exportar((String) comboBox_Tablas.getSelectedItem(), nombreBBDD, "|",
					bbdd.listarColumnas((String) comboBox_BaseDatos.getSelectedItem(),
							(String) comboBox_Tablas.getSelectedItem()),
					bbdd.listarTodo((String) comboBox_BaseDatos.getSelectedItem(),
							(String) comboBox_Tablas.getSelectedItem(), "|"));
		} catch (Exception e) {
			notificaError(null, "Error al exportar", e, "No se ha podido exportar");
			e.printStackTrace();
			return;
		}

		JOptionPane.showMessageDialog(null, "Se ha exportado correctamente");

	}

	@SuppressWarnings("unchecked")
	protected void rellenarColumnas() {

		ArrayList<Columna> columnas;

		list_Columnas.removeAll();

		try {
			columnas = bbdd.listarColumnas((String) comboBox_BaseDatos.getSelectedItem(),
					(String) comboBox_Tablas.getSelectedItem());
		} catch (SQLException e) {
			notificaError(null, "Error al listar", e,
					"Se ha producido un error al realizar una busqueda de las Bases de Datos");
			return;
		}

		Vector<Object> resultadoNombre = new Vector<>();

		for (int i = 0; i < columnas.size(); i++) {
			resultadoNombre.add(columnas.get(i).getNombre() + " - " + columnas.get(i).getTipo());
		}

		list_Columnas.setListData(resultadoNombre);

	}

	protected void rellenarTablas() {

		ArrayList<String> tablas;
		String nombre = (String) comboBox_BaseDatos.getSelectedItem();

		comboBox_Tablas.removeAllItems();

		if (comboBoxCon.getSelectedIndex() == 2) {
			nombre = "";
		}

		try {
			tablas = bbdd.listarTablas(nombre);
		} catch (SQLException e) {
			notificaError(null, "Error al listar", e,
					"Se ha producido un error al realizar una busqueda de las Bases de Datos");
			return;
		}

		for (int i = 0; i < tablas.size(); i++) {
			comboBox_Tablas.addItem(tablas.get(i));
		}

		comboBox_Tablas.setEnabled(true);

	}

	protected void rellenarComboBox() {
		ArrayList<String> basesDatos;
		try {
			basesDatos = bbdd.listarBases();
		} catch (SQLException e) {
			notificaError(null, "Error al listar", e,
					"Se ha producido un error al realizar una busqueda de las Bases de Datos");
			return;
		}

		for (int i = 0; i < basesDatos.size(); i++) {
			comboBox_BaseDatos.addItem(basesDatos.get(i));
		}

	}

	private void conectarBBDD() {
		// Se conecta con la base de datos
		bbdd = new GestorBBDD();

		switch (comboBoxCon.getSelectedIndex()) {
		case 0:
			// Se establece la conexion
			try {
				bbdd.conectarDB(textFieldIP.getText(), textFieldUsuario.getText(), textFieldContra.getText());
			} catch (Exception e) {
				notificaError(null, "Conexion con BBDD", e, "No se ha podido conectar con la base de datos MySQL");
			}
			break;
		case 1:
			// Se establece la conexion
			try {
				bbdd.conectarDB(rutaCon, GestorBBDD.CONEXION_DERBY);
			} catch (Exception e) {
				notificaError(null, "Conexion con BBDD", e, "No se ha podido conectar con la base de datos MySQL");
			}
			break;
		case 2:
			// Se establece la conexion
			try {
				bbdd.conectarDB(rutaCon, GestorBBDD.CONEXION_ACCESS);
			} catch (Exception e) {
				notificaError(null, "Conexion con BBDD", e, "No se ha podido conectar con la base de datos MySQL");
			}
			break;

		default:
			break;
		}

	}

	public static void notificaError(JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido = "";
		if (e != null)
			contenido += e.getClass().getName() + "\n" + e.getMessage(); // Nombre de la excepcion y mensaje de la
																			// excep.
		if (mensaje != null)
			contenido += "\n" + mensaje;
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
	}
}
