package sql3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ExportarImportarBBDD {

	private File ruta;

	public ExportarImportarBBDD(File ruta) {
		super();
		this.ruta = ruta;
	}

	public File getRuta() {
		return ruta;
	}

	public void setRuta(File ruta) {
		this.ruta = ruta;
	}

	public void exportar(String nombreTabla, String nombreBBDD, String delimitadorCampo, ArrayList<Columna> columnas,
			ArrayList<String> contenidoBBDD) throws Exception {

		if (nombreBBDD.isEmpty() || nombreBBDD == null) {
			throw new IllegalArgumentException("Nombre de Base de datos vacia");
		}

		// Creamos el arbol DOM de XML
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		Document doc = docBuilder.newDocument();
		// Fin de creacion de arbol DOM

		// Creacion del elemento RAIZ
		Element raiz = doc.createElement("tabla");
		raiz.setAttribute("nombre", nombreTabla);
		raiz.setAttribute("base", nombreBBDD);
		doc.appendChild(raiz);
		// Fin de creacion de RAIZ

		// Creacion del elemento estructura
		Element estructura = doc.createElement("estructura");
		raiz.appendChild(estructura);
		// Fin de creacion de estructura

		// Creacion de elemento columna
		for (int i = 0; i < columnas.size(); i++) {
			Element columna = doc.createElement("columna");
			columna.setAttribute("nombre", columnas.get(i).getNombre());
			columna.setAttribute("tipo", columnas.get(i).getTipo());
			estructura.appendChild(columna);
		}
		// Fin de creacion elemento columna

		// Creacion del elemento Datos
		Element datos = doc.createElement("datos");
		raiz.appendChild(datos);
		// Fin de creacion de Datos

		// Creacion del elemento Delimitador
		Element delimitador = doc.createElement("delimitador");
		delimitador.setAttribute("valor", delimitadorCampo);
		datos.appendChild(delimitador);
		// Fin de creacion de Delimitador

		// Creacion de elementos Fila
		for (int i = 0; i < contenidoBBDD.size(); i++) {
			Element fila = doc.createElement("fila");
			fila.setTextContent(contenidoBBDD.get(i));
			datos.appendChild(fila);
		}
		// Fin creacion de elementos Fila

		// Creacion de fichero XML
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e1) {
			e1.printStackTrace();
		}
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(ruta + "/" + nombreTabla + ".xml");
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		// Fin de creacion XML

	}

	public ArrayList<String> importarXML(boolean crearBaseDatos, boolean crearTabla, boolean insertarEnMismaTabla,
			String nombreNuevaTabla, boolean usarNombreBaseDatos) throws Exception {
		if (!insertarEnMismaTabla && (nombreNuevaTabla == null || nombreNuevaTabla.trim().isEmpty())) {
			throw new IllegalArgumentException("Introduzca un nombre de tabla");
		}

		if (getNombreBaseDatos().isEmpty() || getNombreBaseDatos() == null) {
			throw new IllegalArgumentException("El nombre de la base de datos está vacia");
		}

		// Se crea el arrayList que contendra todos los sql que se van a ejecutar
		ArrayList<String> resultado = new ArrayList<>();

		// Se abre el documento
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document doc = null;

		try {
			doc = docBuilder.parse(ruta);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		// Fin de abertura de documento

		// Se recoge el nodo raiz
		Element tabla = (Element) (Element) (XPathFactory.newInstance().newXPath().evaluate("/tabla", doc,
				XPathConstants.NODE));

		String nombreTabla;

		// Se establece el nombre de tabla que se va a usar
		if (insertarEnMismaTabla) {
			// El mismo nombre de tabla
			nombreTabla = tabla.getAttribute("nombre");
		} else {
			// Tabla que se ha indicado en los parametros
			nombreTabla = nombreNuevaTabla.trim();
		}

		// Se crea la query para crear la base de datos
		if (crearBaseDatos) {
			resultado.add("CREATE DATABASE " + tabla.getAttribute("base"));
		}

		// ArrayList que inidica si es un varChar o no
		ArrayList<Boolean> elTipoEsVarchar = new ArrayList<>();

		// Se recogen todos los elementos Columna
		NodeList columnas = tabla.getElementsByTagName("columna");

		// Se comprueba el nombre de la base de datos
		String nombreBBDD = tabla.getAttribute("base") + ".";

		if (!usarNombreBaseDatos) {
			nombreBBDD = "";
		}

		// Se crea el inicio de la query
		String createTable = "CREATE TABLE " + nombreBBDD + nombreTabla + "( ";

		// Se recorre las columnas
		for (int i = 0; i < columnas.getLength(); i++) {
			// Se recoge un elemento de la columna
			Node columna = columnas.item(i);

			// Se establece el nombre de la columna recogiendo el valor del atributo Nombre
			createTable += columna.getAttributes().getNamedItem("nombre").getNodeValue() + " ";

			// Se recoge el atributo tipo
			String tipo = columna.getAttributes().getNamedItem("tipo").getNodeValue();

			// Se comprueba si es varchar o Integer
			if (tipo.equalsIgnoreCase("varchar")) {
				// Se añade la longitud
				tipo += "(255)";
				// Se indica que es un varchar
				elTipoEsVarchar.add(true);
			} else {
				// Se inidica que no es un varchar
				elTipoEsVarchar.add(false);
			}

			// Se añade una coma y un fin de parentesis si es el final
			if (i == columnas.getLength() - 1) {
				createTable += tipo + ")";
			} else {
				createTable += tipo + ", ";
			}
		}

		// Se crea la tabla si se especifica
		if (crearTabla) {
			// Se añade la query al resultado
			resultado.add(createTable);
		}

		// Se crea el principio de la query insert
		String insert = "INSERT INTO " + nombreBBDD + nombreTabla + " VALUES";

		// Se recoge el delimitador
		String delimitador = tabla.getElementsByTagName("delimitador").item(0).getAttributes().getNamedItem("valor")
				.getNodeValue().trim();

		// Se recogen todos los nodos fila
		NodeList filas = tabla.getElementsByTagName("fila");

		// Se recorren los nodos fila
		for (int i = 0; i < filas.getLength(); i++) {

			// Se recoge un elemento de la fila
			Node fila = filas.item(i);

			// Se separa por el delimitador
			String[] contenido = fila.getTextContent().split("[" + delimitador + "]");

			// Se alade el parentesis
			insert += "(";

			// Se recorre el contenido del nodo separado
			for (int j = 0; j < contenido.length; j++) {

				// Si es varchar se añaden las comillas
				if (elTipoEsVarchar.get(j)) {
					insert += "'" + contenido[j] + "'";
				} else {
					insert += contenido[j];
				}

				// se añade coma si no es el final
				if (j != contenido.length - 1) {
					insert += ",";
				}
			}

			// Se cierra el parentesis y se alade coma si no es el final
			if (i == filas.getLength() - 1) {
				insert += ")";
			} else {
				insert += "),";
			}

		}

		resultado.add(insert);

		return resultado;
	}

	public String getNombreBaseDatos() throws Exception {
		String resultado = null;

		// Se abre el documento
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document doc = null;

		try {
			doc = docBuilder.parse(ruta);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		// Fin de abertura de documento

		// Se recoge el nodo raiz
		Element tabla = (Element) (Element) (XPathFactory.newInstance().newXPath().evaluate("/tabla", doc,
				XPathConstants.NODE));

		resultado = tabla.getAttribute("base");

		return resultado;
	}

	public String getNombreTabla() throws Exception {
		String resultado = null;

		// Se abre el documento
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document doc = null;

		try {
			doc = docBuilder.parse(ruta);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		// Fin de abertura de documento

		// Se recoge el nodo raiz
		Element tabla = (Element) (Element) (XPathFactory.newInstance().newXPath().evaluate("/tabla", doc,
				XPathConstants.NODE));

		resultado = tabla.getAttribute("nombre");

		return resultado;
	}

}
