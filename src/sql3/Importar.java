package sql3;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Importar extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldRuta;
	private JTextField textFieldNombreNuevaTabla;
	private GestorBBDD bbdd;
	private File ruta;
	ExportarImportarBBDD importarBBDD;
	private JComboBox<Object> comboBoxBaseDatos;
	private JComboBox<Object> comboBoxTabla;
	private JButton okButton;
	boolean crearBaseDatos = false;
	boolean crearTabla = true;

	/**
	 * Create the dialog.
	 */
	public Importar(GestorBBDD bbdd) {
		this.bbdd = bbdd;
		try {
			bbdd.getCon().setAutoCommit(false);
		} catch (SQLException e2) {
			notificaError(null, "Error en autoCommit", e2, "No se ha podido cambiar el autocommit");
		}

		setBounds(100, 100, 634, 384);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 618, 312);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);

		JLabel lblRuta = new JLabel("Ruta");
		lblRuta.setBounds(10, 11, 46, 14);
		contentPanel.add(lblRuta);

		textFieldRuta = new JTextField();
		textFieldRuta.setEditable(false);
		textFieldRuta.setBounds(10, 36, 379, 20);
		contentPanel.add(textFieldRuta);
		textFieldRuta.setColumns(10);

		JButton btnExaminar = new JButton("Examinar");
		btnExaminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser("./ficheros");
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fc.setDialogTitle("Seleccione una ruta");
				int option = fc.showOpenDialog(null);
				if (option == JFileChooser.APPROVE_OPTION) {
					ruta = fc.getSelectedFile();
					textFieldRuta.setText(ruta.getAbsolutePath());

					importarBBDD = new ExportarImportarBBDD(ruta);

					try {
						if (!bbdd.existeBD(importarBBDD.getNombreBaseDatos())) {

							comboBoxBaseDatos.setEnabled(true);
							crearBaseDatos = true;

						}

						if (bbdd.existeTabla(importarBBDD.getNombreBaseDatos(), importarBBDD.getNombreTabla())) {
							comboBoxTabla.setEnabled(true);
							crearTabla = false;
						}

						okButton.setEnabled(true);
						contentPanel.updateUI();

					} catch (Exception e1) {

						notificaError(null, "Error en el XML", e1, "No se ha podido acceder al XML");
					}

				}
			}
		});
		btnExaminar.setBounds(399, 35, 89, 23);
		contentPanel.add(btnExaminar);

		JLabel lbldeseaCrearLa = new JLabel("¿Desea crear la Base Datos?");
		lbldeseaCrearLa.setBounds(10, 86, 214, 14);
		contentPanel.add(lbldeseaCrearLa);

		comboBoxBaseDatos = new JComboBox<Object>();
		comboBoxBaseDatos.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {

				if (comboBoxBaseDatos.getSelectedIndex() == 1) {
					comboBoxTabla.setEnabled(false);
					okButton.setEnabled(false);
					contentPanel.updateUI();
				} else {
					comboBoxTabla.setEnabled(true);
					okButton.setEnabled(true);
					contentPanel.updateUI();
				}

			}
		});
		comboBoxBaseDatos.setEnabled(false);
		comboBoxBaseDatos.setModel(new DefaultComboBoxModel<Object>(new String[] { "Sí", "No" }));
		comboBoxBaseDatos.setBounds(10, 111, 46, 20);
		contentPanel.add(comboBoxBaseDatos);

		JLabel lbldeseaInsertarLos = new JLabel("¿Desea insertar los datos en la tabla existente?");
		lbldeseaInsertarLos.setBounds(283, 86, 298, 14);
		contentPanel.add(lbldeseaInsertarLos);

		comboBoxTabla = new JComboBox<Object>();
		comboBoxTabla.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (comboBoxTabla.getSelectedIndex() == 1) {

					textFieldNombreNuevaTabla.setEnabled(true);
					textFieldNombreNuevaTabla.setEditable(true);

				} else {

					textFieldNombreNuevaTabla.setEnabled(false);
					textFieldNombreNuevaTabla.setEditable(false);

				}

			}
		});
		comboBoxTabla.setModel(new DefaultComboBoxModel<Object>(new String[] { "Sí", "No" }));
		comboBoxTabla.setEnabled(false);
		comboBoxTabla.setBounds(283, 111, 46, 20);
		contentPanel.add(comboBoxTabla);

		JLabel lblNombreDeLa = new JLabel("Nombre de la nueva tabla");
		lblNombreDeLa.setBounds(283, 153, 155, 14);
		contentPanel.add(lblNombreDeLa);

		textFieldNombreNuevaTabla = new JTextField();
		textFieldNombreNuevaTabla.setEnabled(false);
		textFieldNombreNuevaTabla.setEditable(false);
		textFieldNombreNuevaTabla.setBounds(283, 178, 184, 20);
		contentPanel.add(textFieldNombreNuevaTabla);
		textFieldNombreNuevaTabla.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 312, 618, 33);
			getContentPane().add(buttonPane);
			{
				okButton = new JButton("Importar");
				okButton.setEnabled(false);
				okButton.setBounds(389, 5, 110, 23);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						if (comboBoxTabla.getSelectedIndex() == 1
								&& textFieldNombreNuevaTabla.getText().trim().isEmpty() && crearTabla) {
							notificaError(null, "Nombre Tabla", null, "Debe introducir un nombre de tabla");
							return;
						}

						Boolean insertarEnMismaTabla = true;
						String nombreNuevaTabla = null;
						if (comboBoxTabla.getSelectedIndex() == 1) {
							insertarEnMismaTabla = false;
							nombreNuevaTabla = textFieldNombreNuevaTabla.getText().trim();
							crearTabla = true;
						}

						ArrayList<String> r = null;
						try {

							if (bbdd.getTipoConexion() == GestorBBDD.CONEXION_ACCESS
									|| bbdd.getTipoConexion() == GestorBBDD.CONEXION_DERBY) {
								r = importarBBDD.importarXML(false, crearTabla, insertarEnMismaTabla, nombreNuevaTabla,
										false);
							} else {
								r = importarBBDD.importarXML(crearBaseDatos, crearTabla, insertarEnMismaTabla,
										nombreNuevaTabla, true);
							}

							bbdd.ejecutarVariasSentenciasSQL(r);

						} catch (Exception e1) {
							e1.printStackTrace();
							notificaError(null, "Error al Importar", e1, "No se ha podido importar el fichero");
							try {
								bbdd.getCon().rollback();
							} catch (SQLException e2) {
								notificaError(null, "Error en autoCommit", e1, "No se ha podido hacer rollback");
							}
							return;
						}

						try {
							bbdd.getCon().commit();
						} catch (SQLException e1) {
							notificaError(null, "Error en autoCommit", e1, "No se ha podido cambiar el autocommit");
						}

						dispatchEvent(new WindowEvent(getOwner(), WindowEvent.WINDOW_CLOSING));

					}
				});
				buttonPane.setLayout(null);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispatchEvent(new WindowEvent(getOwner(), WindowEvent.WINDOW_CLOSING));

					}
				});
				cancelButton.setBounds(509, 5, 104, 23);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public static void notificaError(JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido = "";
		if (e != null)
			contenido += e.getClass().getName() + "\n" + e.getMessage(); // Nombre de la excepcion y mensaje de la
																			// excep.
		if (mensaje != null)
			contenido += "\n" + mensaje;
		JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
	}
}
